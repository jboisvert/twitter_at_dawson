# twitter_at_dawson

Twitter at Dawson (TAD for short), is a client application for interacting with the social media networking website called Twitter using the unofficial Java library for the Twitter API called Twitter4j and JavaFX for the presentation. 

## How to use

* This project uses Maven to build and run the project (ensure you have this installed). 
* The project has 3 predefined builds, Run, Run without tests, and Tests. If you wish to simply run the app, select Run without tests. 
* You will be prompted with a screen to enter your twitter developer keys (get this from here: https://developer.twitter.com/). Create an account and project if ever one does not exist. 
* Once the user has entered vaid keys, restart the app after submitting. 
* Enjoy the app :) 

## Notes
* In order to be able to save tweets please update database.properties with the required information. 
* This is a school project and attempted to follow the specs specified by Ken Fogel during the course. 
* This project heavily realies on the twitter4j.


<a href="http://twitter4j.org/">
<img src="http://twitter4j.org/en/images/powered-by-badge/powered-by-twitter4j-138x30.png" border="0" width="138" height="30">
</a>
