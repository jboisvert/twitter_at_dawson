package com.jeffreyboisvert.twitteratdawson;

import com.jeffreyboisvert.twitteratdawson.business.Twitter4jPropertiesFileValidator;
import com.jeffreyboisvert.twitteratdawson.presentation.ChangeTwitter4jPropertiesMenuFXMLController;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import javafx.application.Platform;

/**
 *
 * Acts as main start of application. Has area is broken
 * up into parts housing components for the Twitter client. 

 * @author Jeffrey Boisvert
 */
public class MainApp extends Application {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainApp.class);

    //Used to validate the twitter4j.properties file
    private final Twitter4jPropertiesFileValidator validator = new Twitter4jPropertiesFileValidator(TwitterAtDawsonProjectConstants.PATH_TO_TWITTER4J_PROPERTIES);;

    @Override
    public void start(Stage primaryStage) {

        //Get default locale
        Locale currentLocale = Locale.getDefault();

        //If is not present nor valid display form dialog to fill credentials
        //The app ends here and forces restart since in order for the properties file to work
        //when modified a restarted is required. A pop up will be displayed notifyng the user
        //This is a design choice because if not the twitter4j library does not load and verify. 
        if (!(validator.isTwitter4jPropertiesPresent() && validator.isTwitter4jPropertiesValid())) {

            LOG.info("twitter4j.properties is not present or is invalid in MainApp");

            createTwitter4jFormDialog(currentLocale, primaryStage);

        } 
        else {

            createRootLayout(primaryStage, currentLocale);

        }


    }

    /**
     * 
     * Used to create the root layout of the application. It calls the root layout
     * which then create other components that are part of the root layout
     * 
     * @param primaryStage Stage which will be shown to the user
     * 
     */
    private void createRootLayout(final Stage primaryStage, final Locale currentLocale) {

        try {

            //Load rootlayout
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_ROOT));
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
            Parent root = loader.load();

            Scene scene = new Scene(root);
            primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("Title"));

            //Set Twitter icon
            primaryStage.getIcons().add(
                new Image(MainApp.class
                    .getResourceAsStream(TwitterAtDawsonProjectConstants.PATH_TO_APP_ICON)));


            primaryStage.setScene(scene);

            primaryStage.show();

            primaryStage.setResizable(false);

        } catch (IOException | IllegalStateException ex) {

            LOG.error("Major issue loading stage", ex);

        }

    }

    /**
     * 
     * Creates the dialog 
     * 
     * @param currentLocale 
     */
    private void createTwitter4jFormDialog(final Locale currentLocale, Stage primaryStage) {

        //Load Twitter4j form dialogpane
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_TWITTER4J_DIALOG));

        loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));

        try {

            Parent root = loader.load();

            //Disable button so user is forced to enter valid data
            ChangeTwitter4jPropertiesMenuFXMLController controller = loader.getController();

            //Needs to be done or else user cannot close app from login screen
            controller.changeExitButtonToCloseApplication();

            if (root instanceof DialogPane) {

                Dialog dialog = new Dialog();

                dialog.setDialogPane((DialogPane) root);

                //Show the menu and wait until the user is done with it
                dialog.showAndWait();

            } else {

                LOG.error("root item was not a DialogPane in createTwitter4jFormDialog");
                Platform.exit();

            }

        } catch (IOException | IllegalStateException ex) {

            LOG.error("Issue in createTwitter4jFormDialog", ex);

        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}