package com.jeffreyboisvert.twitteratdawson.bean;

import com.jeffreyboisvert.twitteratdawson.presentation.TextValidator;
import java.io.Serializable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * A message bean used when creating a tweet message to send.
 * 
 * @author Jeffrey Boisvert
 */
public final class MessageProperty implements Serializable {

    private final static int DEFAULT_MAX_MESSAGE_LENGTH = 280;

    private final static int DEFAULT_MIN_MESSAGE_LENGTH = 1;

    private final StringProperty message;

    private final IntegerProperty minMessageSize;

    private final IntegerProperty maxMessageSize;

    /**
     * Sets the string property to an empty string. 
     * Sets default to min DEFAULT_MIN_MESSAGE_LENGTH and max DEFAULT_MAX_MESSAGE_LENGTH.
     */
    public MessageProperty() {

        this("", DEFAULT_MIN_MESSAGE_LENGTH, DEFAULT_MAX_MESSAGE_LENGTH);

    }

    /**
     * Used to set the min and max values of the message but set message to an empty string.
     * 
     * @param min message size
     * @param max message size
     */
    public MessageProperty(int min, int max) {

        this("", min, max);

    }

    /**
     * Used to construct the tweet bean with an existing message
     * 
     * @param message  
     * @param min message size
     * @param max message size
     */
    public MessageProperty(String message, int min, int max) {

        this.message = new SimpleStringProperty(message);
        this.minMessageSize = new SimpleIntegerProperty(min);
        this.maxMessageSize = new SimpleIntegerProperty(max);

    }

    /**
     * To retrieve message.
     * 
     * @return current message in tweet 
     */
    public String getMessage() {

        return this.message.get();

    }

    /**
     * To set a new message of the tweet
     * 
     * @param message to assign to tweet
     */
    public void setMessage(String message) {

        this.message.set(message);

    }

    /**
     * Used to append a space and text to the message. 
     * 
     * @param text to append 
     */
    public void appendStringToMessage(String text) {

        String messageText = this.message.get();

        messageText += " " + text;

        this.message.set(messageText);

    }

    /**
     * To get the min message size
     * 
     * @return int of min size
     */
    public int getMinMessageSize() {

        return this.minMessageSize.get();

    }

    /**
     * Used to get the max message size 
     * 
     * @return int of max size
     */
    public int getMaxMessageSize() {

        return this.maxMessageSize.get();

    }

    /**
     * Used to reset the max message text size.
     * 
     * @param max 
     */
    public void setMaxMessageSize(int max) {

        this.maxMessageSize.set(max);

    }

    /**
     * Used to reset the min message text size.
     * 
     * @param min 
     */
    public void setMinMessageSize(int min) {

        this.minMessageSize.set(min);

    }

    /**
     * Used to get a reference to the StringProperty holding the tweet message. 
     * Used to bind with JavaFX nodes. 
     * 
     * @return a reference to the message property of the Tweet.  
     */
    public StringProperty getMessageProperty() {

        return this.message;

    }

    /**
     * Checks if the message has any white space or is empty. 
     * Also validates it is within the bounds specified
     * 
     * @return true if valid false otherwise 
     */
    public boolean isMessageValid() {

        TextValidator textValidator = new TextValidator();

        String text = this.message.get();

        return textValidator.isStringNotSpaceOrBlank(text) && textValidator.isTextLengthWithinBounds(text, this.minMessageSize.get(), this.maxMessageSize.get());

    }

}