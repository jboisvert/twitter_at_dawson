
package com.jeffreyboisvert.twitteratdawson.bean;

import java.util.Date;
import java.util.Objects;

/**
 * Used to encapsulate the data of a Twitter4j Status stored in the database. 
 * 
 * @author Jeffrey Boisvert
 */
public class StatusData {
    
    private long id; 
    
    private Date createdAt; 
        
    private String text; 
    
    private int favoriteCount; 
    
    private int retweetCount; 
        
    private boolean likedByMe;
    
    private boolean retweetedByMe;
    
    /**
     * Default constructor
     */
    public StatusData(){
        
        this(-1, null, "", 0, 0, false, false);
        
    }

    /**
     * Constructor used to set all the fields. 
     * 
     * @param id of the status
     * @param createdAt time of when the status was created
     * @param text of the status
     * @param favioriteCount showing how many likes
     * @param retweetCount 
     * @param likedByMe
     * @param retweetedByMe 
     */
    public StatusData(final long id, final Date createdAt, final String text, 
            final int favioriteCount, final int retweetCount, 
            final boolean likedByMe, final boolean retweetedByMe) {
        
        this.id = id;
        this.createdAt = createdAt;
        this.text = text;
        this.favoriteCount = favioriteCount;
        this.retweetCount = retweetCount;
        this.likedByMe = likedByMe;
        this.retweetedByMe = retweetedByMe;
        
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the value of createdAt
     *
     * @return the value of createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @param createdAt new value of createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Get the value of text
     *
     * @return the value of text
     */
    public String getText() {
        return text;
    }

    /**
     * Set the value of text
     *
     * @param text new value of text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Get the value of favioriteCount
     *
     * @return the value of favioriteCount
     */
    public int getFavoriteCount() {
        return favoriteCount;
    }

    /**
     * Set the value of favioriteCount
     *
     * @param favioriteCount new value of favioriteCount
     */
    public void setFavoriteCount(int favioriteCount) {
        this.favoriteCount = favioriteCount;
    }

    /**
     * Get the value of retweetCount
     *
     * @return the value of retweetCount
     */
    public int getRetweetCount() {
        return retweetCount;
    }

    /**
     * Set the value of retweetCount
     *
     * @param retweetCount new value of retweetCount
     */
    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

    /**
     * Get the value of likedByMe
     *
     * @return the value of likedByMe
     */
    public boolean isLikedByMe() {
        return likedByMe;
    }

    /**
     * Set the value of likedByMe
     *
     * @param likedByMe new value of likedByMe
     */
    public void setLikedByMe(boolean likedByMe) {
        this.likedByMe = likedByMe;
    }

    /**
     * Get the value of retweetedByMe
     *
     * @return the value of retweetedByMe
     */
    public boolean isRetweetedByMe() {
        return retweetedByMe;
    }

    /**
     * Set the value of retweetedByMe
     *
     * @param retweetedByMe new value of retweetedByMe
     */
    public void setRetweetedByMe(boolean retweetedByMe) {
        this.retweetedByMe = retweetedByMe;
    }

    /**
     * Simple to string to help print object instance. 
     * 
     * @return String representing the object. 
     */
    @Override
    public String toString() {
        return "StatusData{" + "id=" + id + ", createdAt=" + createdAt + ", text=" + text + ", favoriteCount=" + favoriteCount + ", retweetCount=" + retweetCount + ", likedByMe=" + likedByMe + ", retweetedByMe=" + retweetedByMe + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 17 * hash + Objects.hashCode(this.createdAt);
        hash = 17 * hash + Objects.hashCode(this.text);
        hash = 17 * hash + this.favoriteCount;
        hash = 17 * hash + this.retweetCount;
        hash = 17 * hash + (this.likedByMe ? 1 : 0);
        hash = 17 * hash + (this.retweetedByMe ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StatusData other = (StatusData) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.favoriteCount != other.favoriteCount) {
            return false;
        }
        if (this.retweetCount != other.retweetCount) {
            return false;
        }
        if (this.likedByMe != other.likedByMe) {
            return false;
        }
        if (this.retweetedByMe != other.retweetedByMe) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.createdAt, other.createdAt)) {
            return false;
        }
        return true;
    }

    
    
}
