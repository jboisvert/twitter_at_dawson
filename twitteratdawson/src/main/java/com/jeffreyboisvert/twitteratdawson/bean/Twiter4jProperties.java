package com.jeffreyboisvert.twitteratdawson.bean;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * Used to hold data of the twitter4j properties file. Fields are not to be null
 * and the object is mutable. 
 * 
 * @author Jeffrey Boisvert
 * 
 */
public final class Twiter4jProperties implements Serializable {

    private final StringProperty consumerKey;
    private final StringProperty consumerSecret;
    private final StringProperty accessToken;
    private final StringProperty accessTokenSecret;

    /**
     * Default Constructor
     */
    public Twiter4jProperties() {
        this("", "", "", "");
    }

    /**
     * 
     * Sets the property fields to the given values. Values must not be null. 
     * 
     * @param consumerKey
     * @param consumerSecret
     * @param accessToken
     * @param accessTokenSecret 
     * @throws NullPointerException if any of the the parameters are null
     */
    public Twiter4jProperties(final String consumerKey, final String consumerSecret, final String accessToken, final String accessTokenSecret) {

        //Ensure not null
        Objects.requireNonNull(consumerKey, "consumerKey must not be null");
        Objects.requireNonNull(consumerSecret, "consumerSecret must not be null");
        Objects.requireNonNull(accessToken, "accessToken must not be null");
        Objects.requireNonNull(accessTokenSecret, "accessTokenSecret must not be null");

        //Safe to assign
        this.consumerKey = new SimpleStringProperty(consumerKey);
        this.consumerSecret = new SimpleStringProperty(consumerSecret);
        this.accessToken = new SimpleStringProperty(accessToken);
        this.accessTokenSecret = new SimpleStringProperty(accessTokenSecret);

    }

    /**
     * @return String value of consumer key
     */
    public String getConsumerKey() {
        return this.consumerKey.get();
    }

    /**
     * @return String value of consumer secret 
     */
    public String getConsumerSecret() {
        return this.consumerSecret.get();
    }

    /**
     * @return String value of access token 
     */
    public String getAccessToken() {
        return this.accessToken.get();
    }

    /**
     * @return String value of the access token secret 
     */
    public String getAccessTokenSecret() {
        return this.accessTokenSecret.get();
    }

    /**
     * @param newConsumerKey
     * @throws NullPointerException if any of the the parameter is null
     */
    public void setConsumerKey(final String newConsumerKey) {

        Objects.requireNonNull(newConsumerKey, "newConsumerKey must not be null");

        this.consumerKey.set(newConsumerKey);
    }

    /**
     * @param newConsumerSecret 
     * @throws NullPointerException if any of the the parameter is null
     */
    public void setConsumerSecret(final String newConsumerSecret) {

        Objects.requireNonNull(newConsumerSecret, "newConsumerSecret must not be null");

        this.consumerSecret.set(newConsumerSecret);
    }

    /**
     * @param newAccessToken 
     * @throws NullPointerException if any of the the parameter is null
     */
    public void setAccessToken(final String newAccessToken) {

        Objects.requireNonNull(newAccessToken, "newAccessToken must not be null");

        this.accessToken.set(newAccessToken);
    }

    /**
     * @param newAccessTokenSecret 
     * @throws NullPointerException if any of the the parameter is null
     */
    public void setAccessTokenSecret(final String newAccessTokenSecret) {

        Objects.requireNonNull(newAccessTokenSecret, "newAccessTokenSecret must not be null");

        this.accessTokenSecret.set(newAccessTokenSecret);
    }

    public StringProperty consumerKeyProperty() {

        return this.consumerKey;

    }

    public StringProperty consumerSecretProperty() {

        return this.consumerSecret;

    }

    public StringProperty accessTokenProperty() {

        return this.accessToken;

    }

    public StringProperty accessTokenSecretProperty() {

        return this.accessTokenSecret;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.consumerKey.get());
        hash = 17 * hash + Objects.hashCode(this.consumerSecret.get());
        hash = 17 * hash + Objects.hashCode(this.accessToken.get());
        hash = 17 * hash + Objects.hashCode(this.accessTokenSecret.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        //Same reference
        if (this == obj) {
            return true;
        }
        //Object is null
        if (obj == null) {
            return false;
        }
        //Not the same class
        if (getClass() != obj.getClass()) {
            return false;
        }

        //Compare objects since assume it is a Twitter4jProperties object
        final Twiter4jProperties other = (Twiter4jProperties) obj;

        if (!Objects.equals(this.consumerKey.get(), other.consumerKey.get())) {
            return false;
        }

        if (!Objects.equals(this.consumerSecret.get(), other.consumerSecret.get())) {
            return false;
        }

        if (!Objects.equals(this.accessToken.get(), other.accessToken.get())) {
            return false;
        }

        return Objects.equals(this.accessTokenSecret.get(), other.accessTokenSecret.get());

    }

    @Override
    public String toString() {
        return "Twitter4jProperties\n{\t" +
            "consumerKey=" + consumerKey.get() +
            "\n\tconsumerSecret=" + consumerSecret.get() +
            "\n\taccessToken=" + accessToken.get() +
            "\n\taccessTokenSecret=" + accessTokenSecret.get() +
            "\n}";
    }
}