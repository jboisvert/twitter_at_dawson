
package com.jeffreyboisvert.twitteratdawson.bean;

import java.util.Objects;

/**
 * Used to encapsulate the data of a user stored in the database. 
 * 
 * @author Jeffrey Boisvert
 */
public class UserData {
    
    private String urlOfProfileImage;
    
    private String name; 
    
    private String screenName; 
    
    private long id; 
    
    /**
     * Default constructor 
     */
    public UserData(){
        
        this(-1, "", "", "");
        
    }
    
    /**
     * Constructor used to set all fields of the class. 
     * @param id
     * @param name
     * @param screenName
     * @param urlOfProfileImage 
     */
    public UserData(final long id, final String name, final String screenName, final String urlOfProfileImage){
        
        this.id = id; 
        this.name = name; 
        this.screenName = screenName; 
        this.urlOfProfileImage = urlOfProfileImage; 
        
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.urlOfProfileImage);
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.screenName);
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    /**
     * Used to check if this object equals another object. 
     * 
     * @param obj to check against
     * @return true if objects are equal or false if they are not. 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserData other = (UserData) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.urlOfProfileImage, other.urlOfProfileImage)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if(Objects.equals(this.screenName, other.screenName)) {
        } else {
            return false;
        }
        return true;
    }

    /**
     * String representation of the UserData object. 
     * @return 
     */
    @Override
    public String toString() {
        return "UserData{" + "urlOfProfileImage=" + urlOfProfileImage + ", name=" + name + ", screenName=" + screenName + ", id=" + id + '}';
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of screenName
     *
     * @return the value of screenName
     */
    public String getScreenName() {
        return screenName;
    }

    /**
     * Set the value of screenName
     *
     * @param screenName new value of screenName
     */
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    /**
     * Get the value of urlOfProfileImage
     *
     * @return the value of urlOfProfileImage
     */
    public String getUrlOfProfileImage() {
        return urlOfProfileImage;
    }

    /**
     * Set the value of urlOfProfileImage
     *
     * @param urlOfProfileImage new value of urlOfProfileImage
     */
    public void setUrlOfProfileImage(String urlOfProfileImage) {
        this.urlOfProfileImage = urlOfProfileImage;
    }
    
}
