
package com.jeffreyboisvert.twitteratdawson.business;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Used to load and manage timelines. 
 * 
 * @author Jeffrey Boisvert
 */
public class HomeTimelineApp implements TwitterTimelineModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(HomeTimelineApp.class);

    private final static int NUMBER_TWEETS_TO_LOAD = 20;

    private final Twitter twitter;

    private int page;

    private final List < TweetModule > timeline;

    /**
     * Default constructor used to set the twitter instance and page to 1 and create an empty list
     */
    public HomeTimelineApp() {

        this.twitter = TwitterFactory.getSingleton();

        this.page = 1;

        this.timeline = new ArrayList < > ();

    }

    /**
     * Used to get the list of tweets. 
     * 
     * @return a list of status objects.  
     */
    @Override
    public List < TweetModule > getTimeline() {

        return this.timeline;

    }

    /**
     * Used to load the main home timeline
     * 
     * @throws TwitterException 
     */
    @Override
    public void loadTimeline() throws TwitterException {

        List < Status > tweetsLoaded = getTimeLine();
        tweetsLoaded.forEach((status) -> {

            timeline.add(new TweetStatus(status));

        });

        //Increase the page for the next time it is called to load more. 
        page += 1;

    }

    /**
     * Loads tweets from the home timeline based on the given page. 
     * By default this loads 20 tweets at a time.
     * 
     * @return
     * @throws TwitterException 
     */
    private List < Status > getTimeLine() throws TwitterException {

        LOG.info("getTimeLine with page " + this.page);

        Paging paging = new Paging();
        paging.setCount(NUMBER_TWEETS_TO_LOAD);
        paging.setPage(page);

        return this.twitter.getHomeTimeline(paging);

    }


}