
package com.jeffreyboisvert.twitteratdawson.business;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Used to load the timeline of mentions (this is one of the timelines shown in notifications). 
 * 
 * @author Jeffrey Boisvert
 */
public class MentionsTimelineApp implements TwitterTimelineModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MentionsTimelineApp.class);

    private final static int NUMBER_TWEETS_TO_LOAD = 20;

    private final Twitter twitter;

    private int page;

    private final List < TweetModule > timeline;

    /**
     * Default constructor used to set the page and default number of tweets to load. 
     */
    public MentionsTimelineApp() {

        this.twitter = TwitterFactory.getSingleton();

        this.page = 1;

        this.timeline = new ArrayList < > ();

    }

    /**
     * Used to load the list of status objections that are mentioning the user. 
     * 
     * @throws TwitterException 
     */
    @Override
    public void loadTimeline() throws TwitterException {

        List < Status > tweetsLoaded = getTimeLine();
        tweetsLoaded.forEach((status) -> {

            timeline.add(new TweetStatus(status));

        });

        //Increase the page for the next time it is called to load more. 
        page += 1;

    }

    /**
     * Used to get the list of tweets. 
     * 
     * @return a list of status objects.  
     */
    @Override
    public List < TweetModule > getTimeline() {

        return this.timeline;

    }

    /**
     * Loads tweets from the home timeline based on the given page. 
     * By default this loads 20 tweets at a time.
     * 
     * @return
     * @throws TwitterException 
     */
    private List < Status > getTimeLine() throws TwitterException {

        LOG.info("get mentions with page " + this.page);

        Paging paging = new Paging();
        paging.setCount(NUMBER_TWEETS_TO_LOAD);
        paging.setPage(page);

        return this.twitter.getMentionsTimeline(paging);

    }

}