package com.jeffreyboisvert.twitteratdawson.business;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.List;
import java.util.Properties;

/**
 * Used to manage interacting with properties files
 * Based on Ken Fogel's PropertyManager class in repo: https://gitlab.com/omniprof/javafx_properties_demo.git
 * 
 * @author Jeffrey Boisvert
 */
public class PropertiesManager {

    /**
     * Loads the Properties object found based on the parameters. 
     *
     * @param path
     * @param propFileName
     * @return a Properties object loaded from the given properties file and path
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException if file is not found
     */
    public final Properties loadTextProperties(final String path, final String propFileName) throws IOException {

        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = Files.newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
        } else {
            //Throw error so prop is not empty 
            throw new FileNotFoundException(propFileName + ".properties was not found at path " + path);
        }

        return prop;
    }

    /**
     * 
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file (excluding file extension)
     * @param prop object holding properties members
     * @throws IOException
     * 
     */
    public final void writeTextProperties(final String path, final String propFileName, final Properties prop) throws IOException {

        /*TODO make the file then store to write it*/
        String pathToWrite = propFileName + ".properties";
        
        // Creates the file or if file exists it is truncated to length of zero before writing
        try (OutputStream propFileStream = new FileOutputStream(pathToWrite)) {

            prop.store(propFileStream, propFileName + "Properties");

        }
    }

    /**
     * 
     * Checks if the properties file specified exists. 
     * 
     * @param path
     * @param propFileName Name of the properties file (excluding file extension)
     * @return true if properties file exists and false if it doesn't exist
     * @throws IOException 
     */
    public final boolean isPropertiesFilePresent(final String path, final String propFileName) throws IOException {

        Path txtFile = get(path, propFileName + ".properties");

        return Files.exists(txtFile);

    }

    /**
     * 
     * Checks if all the keys specified are present and the values associated to them are not 0. 
     * 
     * @param path
     * @param propFileName Name of the properties file (excluding file extension)
     * @param keys list of keys expected to have none 0 values
     * @return true if all keys and value pairs are valid and false otherwise
     * @throws IOException
     */
    public final boolean isPropertiesFileValid(final String path, final String propFileName, List < String > keys) throws IOException {

        //Default is true
        boolean result = true;

        if (isPropertiesFilePresent(path, propFileName)) {

            Properties properties = loadTextProperties(path, propFileName);

            for (String key: keys) {

                String value = properties.getProperty(key);

                //If the value is null or have a length of 0 it is not valid and can break and return false
                if (value == null || value.length() == 0) {

                    result = false;
                    break;

                }

            }

        } else {

            result = false;

        }

        return result;

    }

}