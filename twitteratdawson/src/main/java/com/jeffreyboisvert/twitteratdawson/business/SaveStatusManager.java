
package com.jeffreyboisvert.twitteratdawson.business;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import java.sql.SQLException;

/**
 * Used to handle the logic of state of tweets in the database. 
 * 
 * @author Jeffrey Boisvert
 */
public interface SaveStatusManager {
    
    boolean saveStatus(long userId, UserData author, StatusData status) throws SQLException;
    
    boolean isStatusSaved(long userId, StatusData status) throws SQLException;
    
    boolean removeSavedStatus(long userId, long statusId) throws SQLException;
    
}
