
package com.jeffreyboisvert.twitteratdawson.business;

import com.jeffreyboisvert.twitteratdawson.persistence.TwitterTweetsDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Used to load all the saved tweets in the database. 
 * 
 * @author Jeffrey Boisvert
 */
public class SavedTweetsTimeline implements TwitterTimelineModule{
    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SavedTweetsTimeline.class);

    private final Twitter twitter;

    private final List <TweetModule> timeline;
    
    private final TwitterTweetsDAO dao;

    /**
     * Used to set the dao object used to load the saved tweets.
     * 
     * @param dao to use to retrieve tweets from the database.
     */
    public SavedTweetsTimeline(TwitterTweetsDAO dao){
        
        this.twitter = TwitterFactory.getSingleton();

        this.timeline = new ArrayList <> ();
        
        this.dao = dao; 
        
    }

    /**
     * Used to load more elements into the timeline. 
     * @throws TwitterException 
     */
    @Override
    public void loadTimeline() throws TwitterException {
        
        long userId = this.twitter.getId();
        
        List < TweetModule > tweetsLoaded = getTimeLine(userId);
        
        //Used to clear before adding
        this.timeline.clear();
        
        tweetsLoaded.forEach((status) -> {

            if(!timeline.contains(status)){
                timeline.add(status);
            }


        });

        
    }


    /**
     * Used to get the list of tweets. 
     * 
     * @return a list of TweetModule objects.  
     */
    @Override
    public List<TweetModule> getTimeline() {
        
        return this.timeline;
        
    }
    
    /**
     * Loads tweets from the database based on the page set. 
     * 
     * @return a list of all the tweets retrieved from the database.
     * @throws TwitterException 
     */
    private List < TweetModule > getTimeLine(long userId) throws TwitterException {

        LOG.debug("getTimeLine for " + userId);

        List<TweetModule> tweets = new ArrayList<>();
        
        try {
            
            tweets = this.dao.findAllSavedStatusOfUser(userId);
            
        } catch (SQLException ex) {
            
            LOG.error("Issue occured giving empty list", ex);
        
        }

        return tweets;
        
    }
    
}
