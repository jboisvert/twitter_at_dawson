package com.jeffreyboisvert.twitteratdawson.business;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * This is used to create a list of tweets based on a given query string. 
 * This object is immutable
 * 
 * @author Jeffrey Boisvert
 */
public class SearchTimelineApp implements TwitterTimelineModule{

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserTweetsRetweetedTimelineApp.class);

    private final static int NUMBER_TWEETS_TO_LOAD = 20;

    private final Twitter twitter;

    private final List <TweetModule> timeline;
    
    private final String query;
    
    public SearchTimelineApp(String query){
        
        this.query = query;
        
         this.twitter = TwitterFactory.getSingleton();

        this.timeline = new ArrayList < > ();
        
    }
    
    /**
     * Used to load the timeline with tweets that match the set query string. 
     * 
     * @throws TwitterException 
     */
    @Override
    public void loadTimeline() throws TwitterException {
        
        List < Status > tweetsLoaded = getTimeLine();
        tweetsLoaded.forEach((status) -> {

            timeline.add(new TweetStatus(status));

        });
        
    }

    /**
     * Used to return a reference to the list of status objects.
     * 
     * @return 
     */
    @Override
    public List<TweetModule> getTimeline() {
        
        return this.timeline;
        
    }
    
    /**
     * Loads tweets that match the set query. 
     * 
     * @return list of status objects found
     * @throws TwitterException 
     */
    private List <Status> getTimeLine() throws TwitterException {

        Query queryObject = new Query(this.query);
        QueryResult result = twitter.search(queryObject);
        
        return result.getTweets();

    }
    
}
