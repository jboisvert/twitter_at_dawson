package com.jeffreyboisvert.twitteratdawson.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Used to send direct messages and other useful methods associated to it.
 * Due to the API limitations only 50 messages from the last 30 days can be loaded into memory. 
 * 
 * @author Jeffrey Boisvert
 */
public class SendDirectMessageApp implements SendDirectMessageModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SendDirectMessageApp.class);

    private final Twitter twitter;

    private final List < DirectMessage > allMessages;

    public SendDirectMessageApp() {

        this.twitter = TwitterFactory.getSingleton();

        this.allMessages = new ArrayList < > ();

    }

    /**
     * Send direct message to a user matching the given user handle.
     * It also returns the new message and adds it to the list of all messages loaded
     *
     * @param userHandle
     * @param message
     * @return the DirectMessage object created after sending the message. 
     * @throws TwitterException
     */
    @Override
    public DirectMessage sendMessage(String userHandle, String message) throws TwitterException {

        DirectMessage messageJustSent = this.twitter.sendDirectMessage(userHandle, message);

        this.allMessages.add(messageJustSent);

        return messageJustSent;

    }

    /**
     * Used to load all the available direct messages associated to the logged in user.
     * Get reference to solution here: https://github.com/Twitter4J/Twitter4J/blob/master/twitter4j-examples/src/main/java/twitter4j/examples/directmessage/GetDirectMessages.java
     * to be able to load all messages from the last 30 days. 
     * 
     * @throws TwitterException 
     */
    private void loadAllMessages() throws TwitterException {

        try {

            String cursor = null;
            int count = 50;

            DirectMessageList messages;
            do {

                messages = cursor == null ? twitter.getDirectMessages(count) : twitter.getDirectMessages(count, cursor);
                for (DirectMessage message: messages) {

                    this.allMessages.add(message);

                }

                cursor = messages.getNextCursor();

            } while (messages.size() > 0 && cursor != null);


            sortMessagesNewestToOldest();

        } catch (TwitterException ex) {

            LOG.error("Error loading messages", ex);

        }

    }

    /**
     * Used to load 50 more messages into the collection that is already loaded. 
     * @throws twitter4j.TwitterException when twitter API is unavailable 
     */
    @Override
    public void loadMoreMessages() throws TwitterException {

        //Means it was not previously set so set all messages now
        //Reason set like this so API calls are limited to only when it is needed
        if (this.allMessages.isEmpty()) {

            this.loadAllMessages();

        }
        //Means more messages can be loaded and added
        else {

            List < DirectMessage > moreMessages = this.twitter.getDirectMessages(50);

            moreMessages.forEach((message) -> {

                //If the message has not already been added add it
                if (!this.allMessages.contains(message)) {

                    this.allMessages.add(message);

                }

            });

            sortMessagesNewestToOldest();

        }
    }

    /**
     * Get the direct messages associated to the given user id and current user logged in the app. 
     * 
     * @param userId of the user having
     * @return list of messages of 
     * @throws TwitterException 
     */
    @Override
    public List < DirectMessage > getConversationWithAUser(long userId) throws TwitterException {

        long loggedInUserId = twitter.getId();

        //Means it was not previously set so set all messages now
        //Reason set like this so API calls are limited to only when it is needed
        if (this.allMessages.isEmpty()) {

            this.loadAllMessages();

        }

        //Get messages from user (based on all the messages previously loaded)
        List < DirectMessage > messagesFromUser = this.allMessages.stream()
            //Get messages sent from other user and current user or sent by current user to other user
            .filter(message -> (message.getSenderId() == userId && message.getRecipientId() == loggedInUserId) || (message.getRecipientId() == userId && message.getSenderId() == loggedInUserId))
            .collect(Collectors.toList());

        return messagesFromUser;

    }

    /**
     * Used to ensure the messages are always sorted by newest messages at the bottom of the list (end)
     * and the oldest are at the top (beginning). 
     */
    private void sortMessagesNewestToOldest() {

        Collections.sort(this.allMessages, (DirectMessage directMessageOne, DirectMessage directMessageTwo) -> {

            //Used to ensure no null pointers occur
            if (directMessageOne.getCreatedAt() == null || directMessageTwo.getCreatedAt() == null) {

                return 0;

            }

            //Compare and invert newest to oldest  
            return directMessageOne.getCreatedAt().compareTo(directMessageTwo.getCreatedAt());

        });

    }

    /**
     * Used to get the list of of users
     * Used https://github.com/Twitter4J/Twitter4J/blob/master/twitter4j-examples/src/main/java/twitter4j/examples/friendsandfollowers/GetFriendsIDs.java as reference.
     * 
     * @return A list of users following the logged in user. 
     * @throws TwitterException 
     */
    @Override
    public List < User > getListOfReciptents() throws TwitterException {

        List < User > users = new ArrayList < > ();

        long cursor = -1;
        IDs ids;

        LOG.info("Getting ids");

        do {

            //Gets all users currently following the user
            ids = twitter.getFollowersIDs(cursor);
            for (long id: ids.getIDs()) {

                LOG.info("At " + id);
                users.add(this.twitter.showUser(id));

            }
        } while ((cursor = ids.getNextCursor()) != 0);

        return users;

    }

}