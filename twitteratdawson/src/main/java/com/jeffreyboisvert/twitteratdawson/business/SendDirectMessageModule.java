package com.jeffreyboisvert.twitteratdawson.business;

import java.util.List;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Simple interface for a direct message module
 * 
 * @author Jeffrey Boisvert
 */
public interface SendDirectMessageModule {

    DirectMessage sendMessage(String userHandle, String message) throws TwitterException;

    List getConversationWithAUser(long receiptentUserId) throws TwitterException;

    List < User > getListOfReciptents() throws TwitterException;

    void loadMoreMessages() throws TwitterException;

}