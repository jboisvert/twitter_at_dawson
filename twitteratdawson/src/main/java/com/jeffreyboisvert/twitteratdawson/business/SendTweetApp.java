package com.jeffreyboisvert.twitteratdawson.business;

import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Class that implements the functionality of sending Tweets. 
 * Holds useful methods to send a tweet to twitter. 
 * 
 * @author Jeffrey Boisvert
 */
public class SendTweetApp implements SendTweetModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SendTweetApp.class);

    private final Twitter twitter;

    /**
     * Used to set the Twitter reference needed to send tweets (from twitter4j). 
     */
    public SendTweetApp() {

        //Get single reference to Twitter object in program
        this.twitter = TwitterFactory.getSingleton();

    }

    /**
     * Used to send a tweet
     * 
     * @param tweetMessage holding text of tweet
     * @return Status object holding the information of the Tweet sent 
     * @throws twitter4j.TwitterException 
     */
    @Override
    public Status sendTweet(String tweetMessage) throws TwitterException {

        LOG.debug("sendTweet: " + tweetMessage);
        Status status = this.twitter.updateStatus(tweetMessage);
        return status;

    }

}