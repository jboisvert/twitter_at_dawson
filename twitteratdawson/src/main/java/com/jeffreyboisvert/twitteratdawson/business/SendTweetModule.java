package com.jeffreyboisvert.twitteratdawson.business;

import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * Basic interface for classes to send Twitter Tweets
 * 
 * @author Jeffrey Boisvert
 */
public interface SendTweetModule {

    public Status sendTweet(String tweetMessage) throws TwitterException;

}