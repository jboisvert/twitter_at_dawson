package com.jeffreyboisvert.twitteratdawson.business;

/**
 * Used to add some constraints associated to a Tweet. 
 * This is to ensured it is shared everywhere a tweet is involved.
 * 
 * @author Jeffrey Boisvert
 */
public class TweetContraints {

    public final static int MIN_LENGTH = 1;
    public final static int MAX_LENGTH = 280;
    
}