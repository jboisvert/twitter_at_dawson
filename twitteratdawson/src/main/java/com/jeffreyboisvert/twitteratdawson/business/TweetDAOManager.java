
package com.jeffreyboisvert.twitteratdawson.business;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import com.jeffreyboisvert.twitteratdawson.persistence.TwitterAtDawsonDAO;
import com.jeffreyboisvert.twitteratdawson.persistence.TwitterTweetsDAO;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Used to create the interface needed to save tweets to the local database. 
 * Acts as encapsulation of wanted behaviour. 
 * 
 * @author Jeffrey Boisvert
 */
public class TweetDAOManager implements SaveStatusManager{
    
    private final TwitterTweetsDAO dao; 
    
    /**
     * Default constructor which uses the TwitterAtDawsonDAO object by default as the DAO. 
     * @throws java.io.IOException
     */
    public TweetDAOManager() throws IOException{
        
        this(new TwitterAtDawsonDAO());
        
    }
    
    /**
     * Used to set a TwitterTweetsDAO wanted. 
     * 
     * @param dao given to use.  
     */
    public TweetDAOManager(TwitterTweetsDAO dao){
        
        this.dao = dao; 
        
    }

    /**
     * Used to save the given user data and status data (if needed) and then assiociate the status to the given user id 
     * (if the user is not already in the database). 
     * 
     * @param userId of the logged in user. 
     * @param author of the status
     * @param status to store in the database. 
     * @return true if it was successful or false if no rows were affected. 
     * @throws java.sql.SQLException 
     */
    @Override
    public boolean saveStatus(long userId, UserData author, StatusData status)  throws SQLException{
        
        //Handles if user already exists in database or not. 
        insertUser(userId);
        
        //Handles if the author needs to be inserted or not. 
        insertAuthor(author);
        
        //Handles if the status needs to be inserted or not. 
        insertStatus(status, author.getId());
        
        //Handles creating the relationship with the status and user. 
        int rowsAffected = this.dao.createUserTweetRelationship(status, userId);
        
        //If not 1 it means no rows were affected and the insert did not work. 
        return (rowsAffected == 1);
        
    }
    
    /**
     * Used to insert the user into the database if they do not already exist. 
     * 
     * @param userId to insert into the database. 
     */
    private void insertUser(long userId) throws SQLException{
        
        //If user is not already in database, add them. 
        if(!this.dao.isUserAlreadyCreated(userId)){
            
            this.dao.createUser(userId);
            
        }
        
    }
    
    /**
     * Used to handle the logic of inserting the author object if they do not already exist in the database. 
     * 
     * @param author 
     */
    private void insertAuthor(UserData author) throws SQLException{
        
        //If user is not already in database, add them. 
        if(!this.dao.isAuthorAlreadyCreated(author)){
            
            this.dao.createTweetAuthor(author);
            
        }
        
    }
    
    /**
     * Used to handle the logic of inserting the status object if they do not already exist in the database. 
     * 
     * @param status 
     */
    private void insertStatus(StatusData status, long authorId) throws SQLException{
        
        //If status is not already in database, add them. 
        if(!this.dao.isTweetAlreadyCreated(status)){
            
            this.dao.createStatus(status, authorId);
            
        }
        //Status already exists so update it with the latest info found here
        else {
            this.dao.updateStatus(status);
        }
        
    }

    /**
     * Used to remove the relationship of the given user id and the given status id. 
     * This method assumes the relationship is already in place. 
     * 
     * @param userId
     * @param statusId
     * @return true if it was successful or false if no rows were affected. 
     * @throws java.sql.SQLException 
     */
    @Override
    public boolean removeSavedStatus(long userId, long statusId) throws SQLException{
        
        int rowsAffected = this.dao.deleteUserTweetsRelationship(userId, statusId);
        
        //If not 1 it means no rows were affected. 
        return (rowsAffected == 1);
        
    }

    /**
     * Used to know if the given status is already saved in the database. 
     * 
     * @param userId of the user logged in
     * @param status given to check against the database. 
     * @return true if
     * @throws SQLException 
     */
    @Override
    public boolean isStatusSaved(long userId, StatusData status) throws SQLException {
        
        return this.dao.isTweetAlreadySavedByUser(userId, status.getId());
        
    }
    
}
