package com.jeffreyboisvert.twitteratdawson.business;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Used to hold useful methods for a Tweet module (aka Status object from the twitter4j library). 
 * 
 * @author Jeffrey Boisvert
 */
public interface TweetModule {

    TweetModule replyToTweet(final String reply) throws TwitterException;

    void retweetTweetWithMessage(final String message) throws TwitterException;

    void retweetTweetWithNoMessage() throws TwitterException;

    boolean isTweetRetweetedMe();

    void likeTweet() throws TwitterException;

    void unlikeTweet() throws TwitterException;
    
    void unRetweetTweet() throws TwitterException;

    List < TweetModule > getReplies() throws TwitterException;

    String getUserName();

    String getTweetText();

    int getNumberOfLikes();

    int getNumberOfRetweets();

    int getNumberOfComments() throws TwitterException;

    String getUserProfileImageURL();

    Date getDatePosted();

    String getTwitterHandle();

    long getTweetId();

    User getUserReference();

    boolean isTweetLiked();

    String getTweetURL();
    
    boolean saveStatusToDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException;
    
    boolean deleteStatusFromTheDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException;
    
    boolean isSavedInDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException;

}