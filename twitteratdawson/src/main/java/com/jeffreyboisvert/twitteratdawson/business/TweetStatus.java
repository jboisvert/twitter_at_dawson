package com.jeffreyboisvert.twitteratdawson.business;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Used to represent a tweet from the Twitter social platform. 
 * Holds useful methods and values associated to a tweet. 
 * 
 * @author Jeffrey Boisvert
 */
public class TweetStatus implements TweetModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetStatus.class);

    private final Twitter twitter;

    private final UserData user; 
    
    private final StatusData status; 

    private final String tweetURL;
    
    /**
     * Passed the Status (from twitter4j library) to construct tweet object
     * 
     * @param status of the current tweet to encapsulate
     */
    public TweetStatus(final Status status) {

        
        this.status = new StatusData(status.getId(), status.getCreatedAt(), 
                                     status.getText(), status.getFavoriteCount(), status.getRetweetCount(), 
                                     status.isFavorited(), status.isRetweetedByMe());

        final User statusUser = status.getUser();
        this.user = new UserData(statusUser.getId(), statusUser.getName(), statusUser.getScreenName(), statusUser.getProfileImageURL());

        this.twitter = TwitterFactory.getSingleton();

        this.tweetURL = "https://twitter.com/" + this.user.getScreenName() + "/status/" + this.status.getId();

    }
    
    /**
     * Used to set the Tweet object based on given data beans. 
     * 
     * @param status bean holding all data about the status.
     * @param user bean holding all the data about a user.
     */
    public TweetStatus(final StatusData status, final UserData user){
        
        this.status = status; 
        
        this.user = user; 
        
        this.twitter = TwitterFactory.getSingleton();

        this.tweetURL = "https://twitter.com/" + this.user.getScreenName() + "/status/" + this.status.getId();

    }

    /**
     * Retrieve the user's name of the tweet.
     * 
     * @return the user's name of the tweet. 
     */
    @Override
    public String getUserName() {

        return this.user.getName();

    }

    /**
     * Get the tweet's text
     * 
     * @return string of the tweet posted
     */
    @Override
    public String getTweetText() {

        return this.status.getText();

    }

    /**
     * Used to return the URL of the user's profile image. 
     * 
     * @return the URL of the user's profile image 
     */
    @Override
    public String getUserProfileImageURL() {

        return this.user.getUrlOfProfileImage();

    }

    /**
     * Used to retrieve the data when the tweet was posted
     * 
     * @return the date the tweet was posted in String form
     */
    @Override
    public Date getDatePosted() {

        return this.status.getCreatedAt();

    }

    /**
     * To retrieve the user twitter handle associated to the tweet in question
     * 
     * @return a String of the user's handle (ex: @johndoe12345) 
     */
    @Override
    public String getTwitterHandle() {

        return this.user.getScreenName();

    }

    /**
     * Used to retrieve the status id of the tweet. 
     * 
     * @return the id of the tweet status
     */
    @Override
    public long getTweetId() {

        return this.status.getId();

    }

    /**
     * Pass a reference to the user object associated to the tweet. 
     * 
     * @return a twitter4j User object associated to the given tweet. If there is no user returns null
     */
    @Override
    public User getUserReference() {
        
        try {
            return this.twitter.showUser(this.user.getId());
        } catch (TwitterException ex) {
            LOG.debug("No user of id " + this.user.getId());
            return null;
        }

    }

    /**
     * Used to reply to the given tweet with the given reply message.
     * 
     * @param reply message to sent. 
     * @return the tweet module made 
     * @throws TwitterException 
     */
    @Override
    public TweetModule replyToTweet(final String reply) throws TwitterException {

        StatusUpdate statusUpdate = new StatusUpdate("@" + this.user.getScreenName() + " " + reply);

        statusUpdate.setInReplyToStatusId(this.status.getId());

        Status replyTweet = this.twitter.updateStatus(statusUpdate);

        //Return newly created tweet
        return new TweetStatus(replyTweet);

    }

    /**
     * A retweet with a message follows this format: 
     * [Message]
     * [URL] 
     * 
     * Where the [URL] is formatted as https://twitter.com/(userHandle)/status/(tweetId)
     * 
     * The message is still subject to the limits (to be validated before sending) but the URL is then appended.
     * The URL is counted towards the character limit. 
     * This means the limit for the message is the (MAX_LIMIT - URL length) - 1 to include the space between the message
     * and the URL. 
     * 
     * @param message to send
     * @throws TwitterException when an issue occurred sending the tweet. 
     */
    @Override
    public void retweetTweetWithMessage(final String message) throws TwitterException {

        String fullRetweetMessage = message + " " + this.tweetURL;

        this.twitter.updateStatus(fullRetweetMessage);

    }

    /**
     * Used to retweet a tweet with no message. 
     * 
     * @throws TwitterException 
     */
    @Override
    public void retweetTweetWithNoMessage() throws TwitterException {

        this.twitter.retweetStatus(this.status.getId());
        this.status.setRetweetedByMe(true);
        
        int newCount = this.status.getRetweetCount() + 1;
        this.status.setRetweetCount(newCount);

    }

    /**
     * Used to like the given tweet. 
     * @throws TwitterException 
     */
    @Override
    public void likeTweet() throws TwitterException {

        this.twitter.createFavorite(this.getTweetId());
        this.status.setLikedByMe(true);
        
        int newCount = this.status.getFavoriteCount() + 1;
        this.status.setFavoriteCount(newCount);

    }

    /**
     * Used to unlike the given tweet.
     * @throws TwitterException 
     */
    @Override
    public void unlikeTweet() throws TwitterException {

        this.twitter.destroyFavorite(this.getTweetId());
        this.status.setLikedByMe(false);
        
        int newCount = this.status.getFavoriteCount() - 1;
        this.status.setFavoriteCount(newCount);

    }

    /**
     * Used to get the number of likes. 
     * 
     * @return like count of tweet. 
     */
    @Override
    public int getNumberOfLikes() {

        return this.status.getFavoriteCount();

    }

    /**
     * Used to get the number of retweets. 
     * 
     * @return retweet count of tweet. 
     */
    @Override
    public int getNumberOfRetweets() {

        return this.status.getRetweetCount();

    }

    /**
     * Used to get the number of comments associated with the Tweet. 
     * 
     * @return number of comments 
     * @throws twitter4j.TwitterException 
     */
    @Override
    public int getNumberOfComments() throws TwitterException {

        //Used to build the query to find tweets that replied to this tweet. 
        Query query = new Query("to:" + this.getTwitterHandle());

        query.setSinceId(this.status.getId());

        QueryResult results = twitter.search(query);

        //Used to get the number of replies to the Tweet. 
        return results.getTweets().size();

    }

    /**
     * Used to get a list of Tweets that replied to this tweet.Based on code found here: https://stackoverflow.com/questions/11969896/twitter4j-get-list-of-replies-for-a-certain-tweet
     * 
     * @return a list of Tweets that replied to the given tweet.  
     * @throws twitter4j.TwitterException  
     */
    @Override
    public List < TweetModule > getReplies() throws TwitterException {

        List < TweetModule > replies = new ArrayList < > ();

        //Used to build the query to find tweets that replied to this tweet. 
        Query query = new Query("to:" + this.getTwitterHandle());

        query.setSinceId(this.status.getId());

        QueryResult results;

        do {

            results = twitter.search(query);

            LOG.info("Results size: " + results.getTweets().size());
            List < Status > tweets = results.getTweets();

            //Could easily be a foreach loop but wanted to practice functional programming.
            tweets.stream()
                //Tweet is a reply to this tweet.
                .filter((tweet) -> (tweet.getInReplyToStatusId() == this.getTweetId()))
                .forEachOrdered((tweet) -> {
                    replies.add(new TweetStatus(tweet));
                });

        } while ((query = results.nextQuery()) != null);

        return replies;

    }

    /**
     * Check if the tweet has been liked or not. 
     * 
     * @return true if the tweet is liked by user.  
     */
    @Override
    public boolean isTweetLiked() {

        return this.status.isLikedByMe();

    }

    /**
     * Used to know if the user has already retweeted the tweet or not. 
     * 
     * @return true if the user has already retweeted the tweet. 
     */
    @Override
    public boolean isTweetRetweetedMe() {

        return this.status.isRetweetedByMe();

    }

    /**
     * Used to get the URL of the tweet. 
     * 
     * @return a string version of the tweet URL which follows the following format: 
     * https://twitter.com/(userHandle)/status/(tweetId) excluding the brackets. 
     * 
     */
    @Override
    public String getTweetURL() {
        
        return this.tweetURL;

    }

    /**
     * Simple to string to show all the variables in the tweet. 
     * 
     * @return string of the tweet
     */
    @Override
    public String toString() {
        return "Tweet{" + "twitter=" + twitter + ", status=" + status + ", user=" + user + ", tweetURL=" + tweetURL + '}';
    }

    @Override
    public void unRetweetTweet() throws TwitterException {
        
        this.twitter.unRetweetStatus(this.status.getId());
        this.status.setRetweetedByMe(false);
        
        int newCount = this.status.getRetweetCount() - 1;
        this.status.setRetweetCount(newCount);
        
    }

    /**
     * Used to save this status to a database handled by the given manager. 
     * 
     * @param saveManager given handling where to save it to. 
     * @return true if it was successful and false otherwise. 
     * @throws java.sql.SQLException 
     * @throws twitter4j.TwitterException if there was a issue getting the logged in user's id. 
     */
    @Override
    public boolean saveStatusToDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException{
        
        return saveManager.saveStatus(this.twitter.getId(), this.user, this.status);
        
    }

    /**
     * Used to delete this status to a database handled by the given manager. 
     * 
     * @param saveManager given handling where to delete it from. 
     * @return true if it was successful and false otherwise. 
     * @throws java.sql.SQLException 
     * @throws twitter4j.TwitterException if there was a issue getting the logged in user's id. 
     */
    @Override
    public boolean deleteStatusFromTheDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException {
        
        return saveManager.removeSavedStatus(this.twitter.getId(), this.status.getId());
        
    }

    /**
     * Used to know if the tweet is saved in the database handled by the given manager. 
     * 
     * @param saveManager given handling where to look. 
     * @return true if it is already stored and false otherwise. 
     * @throws SQLException 
     */
    @Override
    public boolean isSavedInDatabase(SaveStatusManager saveManager) throws SQLException, TwitterException {
        
        return saveManager.isStatusSaved(twitter.getId(), this.status);
        
    }

}