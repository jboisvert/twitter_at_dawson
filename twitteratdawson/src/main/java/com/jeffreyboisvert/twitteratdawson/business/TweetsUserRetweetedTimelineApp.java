
package com.jeffreyboisvert.twitteratdawson.business;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * App used to have all the tweets that the user retweeted. 
 * 
 * @author Jeffrey Boisvert
 */
public class TweetsUserRetweetedTimelineApp implements TwitterTimelineModule {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetsUserRetweetedTimelineApp.class);

    private final static int NUMBER_TWEETS_TO_LOAD = 20;

    private final Twitter twitter;

    private int page;

    private final List < TweetModule > timeline;

    /**
     * Default constructor used to initialize everything needed to for timeline app
     */
    public TweetsUserRetweetedTimelineApp() {

        this.twitter = TwitterFactory.getSingleton();

        this.page = 1;

        this.timeline = new ArrayList < > ();

    }

    /**
     * Used to populate the list of tweets with all the tweets of the user that was a retweet
     * A retweet of the user means the status returns true for isRetweet. 
     * 
     * @throws TwitterException 
     */
    @Override
    public void loadTimeline() throws TwitterException {

        List < Status > tweetsLoaded = getTimeLine();
        tweetsLoaded.forEach((status) -> {

            //Only add if it is a retweet
            if (status.isRetweet()) {
                timeline.add(new TweetStatus(status));
            }

        });

        //Increase the page for the next time it is called to load more. 
        page += 1;

    }

    /**
     * Used to get the list of tweets. 
     * 
     * @return a list of status objects.  
     */
    @Override
    public List < TweetModule > getTimeline() {

        return this.timeline;

    }

    /**
     * Loads user timeline tweets (profile)
     * By default this loads 20 tweets at a time. 
     * 
     * @return
     * @throws TwitterException 
     */
    private List < Status > getTimeLine() throws TwitterException {

        LOG.info("get mentions with page " + this.page);

        Paging paging = new Paging();
        paging.setCount(NUMBER_TWEETS_TO_LOAD);
        paging.setPage(page);

        return this.twitter.getUserTimeline(paging);

    }

}