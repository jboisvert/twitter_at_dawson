package com.jeffreyboisvert.twitteratdawson.business;

import com.jeffreyboisvert.twitteratdawson.MainApp;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Used to hold logic for the twitter4j.properties file
 * 
 * @author Jeffrey Boisvert
 */
public final class Twitter4jPropertiesFileValidator {

    private final String pathToTwitter4jProperties;

    //Constants
    private final String CONSUMER_KEY = "oauth.consumerKey";
    private final String CONSUMER_SECRET = "oauth.consumerSecret";
    private final String ACCESS_TOKEN = "oauth.accessToken";
    private final String ACCESS_TOKEN_SECRET = "oauth.accessTokenSecret";
    private final String FILE_NAME = "twitter4j";

    /**
     * 
     * Assigns path used to find the file
     * 
     * @param path 
     */
    public Twitter4jPropertiesFileValidator(String path) {

        Objects.requireNonNull(path, "given parameter cannot be null");

        this.pathToTwitter4jProperties = path;

    }

    /**
     * 
     * Checks if the twitter4j file is present
     * 
     * @return true if the file is present and false otherwise 
     */
    public boolean isTwitter4jPropertiesPresent() {

        PropertiesManager propertiesManager = new PropertiesManager();

        //false by default
        boolean result = false;

        try {

            result = propertiesManager.isPropertiesFilePresent(this.pathToTwitter4jProperties, this.FILE_NAME);

        } catch (IOException ex) {

            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, "error in isTwitter4jPropertiesPresent", ex);

        }

        return result;

    }

    /**
     * 
     * Checks to see if the Twitter4j properties file is valid
     * 
     * @return true if the properties file is valid with keys and values not having a length of 0 
     */
    public boolean isTwitter4jPropertiesValid() {

        PropertiesManager propertiesManager = new PropertiesManager();

        //false by default
        boolean result = false;

        try {

            //If it is not present it is not valid so no need to continue
            if (isTwitter4jPropertiesPresent()) {

                //Assign keys
                ArrayList < String > keys = new ArrayList < > ();
                keys.add(this.CONSUMER_KEY);
                keys.add(this.CONSUMER_SECRET);
                keys.add(this.ACCESS_TOKEN);
                keys.add(this.ACCESS_TOKEN_SECRET);

                result = propertiesManager.isPropertiesFileValid(this.pathToTwitter4jProperties, this.FILE_NAME, keys);

            }

        } catch (IOException ex) {

            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, "error in isTwitter4jPropertiesPresent", ex);

        }

        return result;

    }

    public String getPathToTwitter4jProperties() {
        return this.pathToTwitter4jProperties;
    }

    public String getConsumerKeyLabel() {
        return this.CONSUMER_KEY;
    }

    public String getConsumerSecretLabel() {
        return this.CONSUMER_SECRET;
    }

    public String getAccessToken() {
        return this.ACCESS_TOKEN;
    }

    public String getAccessTokenSecret() {
        return this.ACCESS_TOKEN_SECRET;
    }

    public String getFileName() {
        return this.FILE_NAME;
    }

}