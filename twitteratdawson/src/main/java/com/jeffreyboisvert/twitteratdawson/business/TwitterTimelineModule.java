package com.jeffreyboisvert.twitteratdawson.business;

import java.util.List;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * Used to build a twitter timeline module.
 * 
 * @author Jeffrey Boisvert
 */
public interface TwitterTimelineModule {

    void loadTimeline() throws TwitterException;

    List <TweetModule> getTimeline();

}