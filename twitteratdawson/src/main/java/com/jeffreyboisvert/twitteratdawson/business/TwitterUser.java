
package com.jeffreyboisvert.twitteratdawson.business;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Used to hold useful methods and information about a Twitter User
 * 
 * @author Jeffrey Boisvert
 */
public class TwitterUser implements TwitterUserModule{

    private final User user;
    
    private final Twitter twitter; 
    
    /**
     * Used to set the user being encapsulated and other set ups.
     * @param user 
     */
    public TwitterUser(User user){
        
        this.user = user;
        
        this.twitter = TwitterFactory.getSingleton();
        
    }

    /**
     * Used to unfollow this user.
     * 
     * @throws TwitterException 
     */
    @Override
    public void unfollow() throws TwitterException {
        
        this.twitter.destroyFriendship(this.user.getId());
        
    }

    /**
     * Used to follow this user. 
     * 
     * @throws TwitterException 
     */
    @Override
    public void follow() throws TwitterException {
        
        this.twitter.createFriendship(this.user.getId());
        
    }

    /**
     * Used to know if the user is following the authenticated user.
     * 
     * @return true if the user is following the authenticated user.
     * @throws TwitterException 
     */
    @Override
    public boolean isFollowingAuthenticatedUser() throws TwitterException {

        return this.twitter.showFriendship(this.user.getScreenName(),twitter.getScreenName()).isSourceFollowingTarget();
        
    }

    /**
     * Used to know if the user is being followed by the authenticated user.
     * 
     * @return true if the user is being followed by the authenticated user. 
     * @throws TwitterException 
     */
    @Override
    public boolean isFollowedByAuthenticatedUser() throws TwitterException {

        return this.twitter.showFriendship(twitter.getScreenName(), this.user.getScreenName()).isSourceFollowingTarget();

    }

    /**
     * Used to get the user id. 
     * 
     * @return long of the user id. 
     */
    @Override
    public long getId() {
        
        return this.user.getId();
        
    }

    /**
     * Used to get the twitter handle of the user. 
     * 
     * @return the twitter handle of the user. 
     */
    @Override
    public String getHandle() {
        
        return this.user.getScreenName();
        
    }

    /**
     * Used to get the description of the user. 
     * 
     * @return the description of the user found in their profile.
     */
    @Override
    public String getDescription() {
        
        return this.user.getDescription();
        
    }

    /**
     * Gets the name of the user. 
     * 
     * @return the user's name.  
     */
    @Override
    public String getName() {
        
        return this.user.getName();
        
    }

    /**
     * Used to get the URL to the user's profile image. 
     * 
     * @return URL to the user's profile image. 
     */
    @Override
    public String getProfileImageURL() {
        
        return this.user.getProfileImageURL();
        
    }

    /**
     * Used to get a 400 x 400 user profile image. 
     * To be used if the get profile image is too small. 
     * 
     * @return URL to the user's profile image. 
     */
    @Override
    public String getProfileImage400X400URL() {
        
        return this.user.get400x400ProfileImageURL();
        
    }

    /**
     * Used to know if the user reference is the same as the authenticating user. 
     * @return
     * @throws TwitterException 
     */
    @Override
    public boolean isTheAuthenticatingUser() throws TwitterException {
        
        return this.user.getId() == this.twitter.getId();
        
    }
    
}
