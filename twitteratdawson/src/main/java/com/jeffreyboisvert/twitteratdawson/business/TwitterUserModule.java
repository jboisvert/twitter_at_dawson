
package com.jeffreyboisvert.twitteratdawson.business;

import twitter4j.TwitterException;

/**
 * Used to encapsulate wanted behvaiour of a user. 
 * 
 * @author Jeffrey Boisvert
 */
public interface TwitterUserModule {
    
    void unfollow() throws TwitterException;
    
    void follow() throws TwitterException;
    
    long getId();
    
    String getHandle();
    
    String getDescription();
    
    String getName();
    
    String getProfileImageURL(); 
    
    String getProfileImage400X400URL();
    
    boolean isFollowingAuthenticatedUser() throws TwitterException;
    
    boolean isFollowedByAuthenticatedUser() throws TwitterException;
    
    boolean isTheAuthenticatingUser() throws TwitterException;
    
}
