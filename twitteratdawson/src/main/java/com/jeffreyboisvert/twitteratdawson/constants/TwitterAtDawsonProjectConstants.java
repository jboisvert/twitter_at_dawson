package com.jeffreyboisvert.twitteratdawson.constants;

/**
 * Used to hold constants needed for the twitter at Dawson 
 * project. All project constants can be found here including file paths 
 * among other thing things. 
 * 
 * @author Jeffrey Boisvert
 */
public final class TwitterAtDawsonProjectConstants {

    /**
     * Private constructor so this class cannot be initialized
     */
    private TwitterAtDawsonProjectConstants() {
        //This class cannot be instantiated
    }

    public final static String PATH_TO_TWITTER4J_PROPERTIES = "";
    public final static String TWITTER4J_PROPERTIES_FILE_NAME = "twitter4j";
    public final static String PATH_TO_FXML_TWITTER4J_DIALOG = "/fxml/ChangeTwitter4jPropertiesMenuFXML.fxml";
    public final static String PATH_TO_FXML_DIRECT_MESSAGE = "/fxml/DirectMessageFXML.fxml";
    public final static String PATH_TO_FXML_TWEET_MENTIONS = "/fxml/TweetMentionsFXML.fxml";
    public final static String PATH_TO_FXML_RETWEETS = "/fxml/RetweetsFXML.fxml";
    public final static String PATH_TO_FXML_SEARCH = "/fxml/SearchTweetsFXML.fxml";
    public final static String PATH_TO_FXML_TIMELINE_MODULE = "/fxml/TimelineTwitterModuleFXML.fxml";
    public final static String PATH_TO_FXML_SETTINGS = "/fxml/SettingsFXML.fxml";
    public final static String PATH_TO_FXML_TWEET = "/fxml/TweetFXML.fxml";
    public final static String PATH_TO_FXML_MAIN_LEFT_MENU = "/fxml/MainLeftMenuFXML.fxml";
    public final static String PATH_TO_FXML_ROOT = "/fxml/RootLayoutFXML.fxml";
    public final static String PATH_TO_FXML_USER_TILE_FXML = "/fxml/UserTileFXML.fxml";
    public final static String PATH_TO_FXML_SEND_TWEETS = "/fxml/SendTweetFXML.fxml";
    public final static String PATH_TO_APP_ICON = "/images/Twitter_Logo_WhiteOnBlue.png";
    public final static String FATAL_ERROR_MESSAGE_KEY = "Major_Issue_Closing_App";
    public final static String PATH_TO_TIMELINE_LIST_VIEW_FXML = "/fxml/TimelineListView.fxml";
    public final static String PATH_TO_HELP_PAGE = "/fxml/HelpDialogFXML.fxml";
    public final static String PATH_TO_SAVED_TWEETS_TIMELINE_MODULE_FXML = "/fxml/SavedTweetsTimelineModuleFXML.fxml";

}