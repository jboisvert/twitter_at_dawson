
package com.jeffreyboisvert.twitteratdawson.persistence;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import com.jeffreyboisvert.twitteratdawson.business.PropertiesManager;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.business.TweetStatus;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to interact with the Database for the Twitter at Dawson project which is used to save tweets 
 * based on the logged in user. 
 * 
 * @author Jeffrey Boisvert
 */
public class TwitterAtDawsonDAO implements TwitterTweetsDAO{
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterAtDawsonDAO.class);

    private final String url;
    private final String username;
    private final String password;
    
    /**
     * Default constructor used to load database.properties in the root of the jar. 
     * 
     * @throws IOException 
     */
    public TwitterAtDawsonDAO() throws IOException{
        
        PropertiesManager propertiesManager = new PropertiesManager();

        Properties databaseProperties = propertiesManager.loadTextProperties("", "database");
            
        this.url = databaseProperties.getProperty("url");
        this.username = databaseProperties.getProperty("user");
        this.password = databaseProperties.getProperty("password"); 
        
    }
    
    /**
     * Used to assign the TwitterAtDawsonDAO object wanted url, user and password to 
     * access the database to interact with. Ensure the database is structured correctly or else error will occur. 
     * 
     * @param url
     * @param user
     * @param password 
     */
    public TwitterAtDawsonDAO(final String url, final String user, final String password){
        
        this.url = url; 
        this.username = user; 
        this.password = password;
        
    }
    
    

    /**
     * Used to create a status in the database tweet table. This assumes the user is already in the database. 
     * 
     * @param statusData holding data about the status. 
     * @param authorId that holds the user id of the author of the tweet
     * @return number of rows affected (should be 1 if successful)
     * @throws SQLException 
     */
    @Override
    public int createStatus(StatusData statusData, long authorId) throws SQLException {
        
        LOG.info("Attempting to insert a status " + statusData + " by id " + authorId);
        
        int result;
        String createQuery = "INSERT INTO TWEETS (TWEET_ID, TWITTER_ID, CREATED_AT, TEXT, FAV_COUNT, RETWEET_COUNT) VALUES (?,?,?,?,?,?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);

            PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setLong(1, statusData.getId());
            ps.setLong(2, authorId);
            
            //Solution from https://stackoverflow.com/questions/2400955/how-to-store-java-date-to-mysql-datetime
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(statusData.getCreatedAt());
            
            ps.setString(3, currentTime);
            ps.setString(4, statusData.getText());
            ps.setInt(5, statusData.getFavoriteCount());
            ps.setInt(6, statusData.getRetweetCount());

            result = ps.executeUpdate();
            
        }
        
        LOG.info("# of records created : " + result);
        return result;
        
    }

    /**
     * Used to create a tweet author in the database
     * @param userData holding the data about the author. 
     * @return number of rows affected (should be 1 if successful)
     * @throws SQLException 
     */
    @Override
    public int createTweetAuthor(UserData userData) throws SQLException {
        
        LOG.info("Attempting to insert an author " + userData);

        
        int result;
        String createQuery = "INSERT INTO TWEET_AUTHORS (TWITTER_ID, NAME, SCREEN_NAME, IMAGE_URL) VALUES (?,?,?,?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);

        PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            
            ps.setLong(1, userData.getId());
            ps.setString(2, userData.getName());
            ps.setString(3, userData.getScreenName());
            ps.setString(4, userData.getUrlOfProfileImage());

            result = ps.executeUpdate();
            
        }
        
        LOG.info("# of records created : " + result);
        return result;
        
    }

    /**
     * Used to insert the id of the logged in user. 
     * 
     * @param twitterId of the logged in user. 
     * @return number of rows affected (should be 1 if successful)
     * @throws SQLException 
     */
    @Override
    public int createUser(long twitterId) throws SQLException {
        
        LOG.info("Attempting to insert user " + twitterId);
        
        int result;
        String createQuery = "INSERT INTO USERS (TWITTER_ID) VALUES (?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);

        PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            
            ps.setLong(1, twitterId);

            result = ps.executeUpdate();
            
        }
        
        LOG.info("# of records created : " + result);
        return result;
        
    }

    /**
     * This is used to insert the relation between a tweet and user meaning the logged in user decided to 
     * save a tweet to view later.This method assumes the tweet and user have already been inserted into the database before creating relationship 
     * 
     * @param statusData of the tweet being saved. 
     * @param userId of the logged in user. 
     * @return number of rows affected (should be 1)
     * @throws java.sql.SQLException 
     */
    @Override
    public int createUserTweetRelationship(StatusData statusData, long userId) throws SQLException{
        
        int result;
        String createQuery = "INSERT INTO TWEETS_USERS (TWITTER_ID, TWEET_ID, LIKED, REWTWEETED) VALUES (?, ?, ? ,?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);

        PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            
            ps.setLong(1, userId);
            ps.setLong(2, statusData.getId());
            ps.setBoolean(3, statusData.isLikedByMe());
            ps.setBoolean(4, statusData.isRetweetedByMe());

            result = ps.executeUpdate();
            
        }
        
        LOG.info("# of records created : " + result);
        return result;
        
        
    }

    /**
     * This returns all the tweets that belong to the given userId in the database. 
     * This looks at the tweets the user has saved. 
     * 
     * @param userId of the logged in user. 
     * @return a list of TweetModule made from objects found in the database. 
     * @throws SQLException 
     */
    @Override
    public List<TweetModule> findAllSavedStatusOfUser(long userId) throws SQLException {
        
        LOG.info("Finding tweets saved by " + userId);
        
        List<TweetModule> rows = new ArrayList<>();

        String selectQuery = "SELECT T.TWEET_ID, T.CREATED_AT, T.TEXT, T.FAV_COUNT, T.RETWEET_COUNT, TU.LIKED, TU.REWTWEETED, TA.TWITTER_ID, TA.NAME, TA.SCREEN_NAME, TA.IMAGE_URL "
                            + "FROM TWEETS T "
                            + "JOIN TWEETS_USERS TU ON T.TWEET_ID = TU.TWEET_ID "
                            + "JOIN TWEET_AUTHORS TA ON T.TWITTER_ID = TA.TWITTER_ID "
                            + "WHERE TU.TWITTER_ID = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            pStatement.setLong(1, userId);
            // A new try-with-resources block begins for creating the ResultSet
            // object
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(createTweetModuleFromRow(resultSet));
                }
            }
        }
        
        LOG.info("# of records found : " + rows.size());
        return rows;    
    
    }
    
    /**
     * Helper method to create a TweetModule object. It handles creating the StatusData and UserData objects. 
     * @param resultSet
     * @return a TweetModule object of the given result set. 
     * @throws SQLException 
     */
    private TweetModule createTweetModuleFromRow(ResultSet resultSet) throws SQLException{
        
        //User
        long userId = resultSet.getLong("TWITTER_ID");
        String name = resultSet.getString("NAME");
        String screenName = resultSet.getString("SCREEN_NAME");
        String imageURL = resultSet.getString("IMAGE_URL");
        
        UserData user = new UserData(userId, name, screenName, imageURL);
        
        //Status
        long statusId = resultSet.getLong("TWEET_ID");
        Date createdAt = resultSet.getDate("CREATED_AT");
        String text = resultSet.getString("TEXT");
        int favoriteCount = resultSet.getInt("FAV_COUNT");
        int retweetCount = resultSet.getInt("RETWEET_COUNT");
        boolean likedByMe = resultSet.getBoolean("LIKED");
        boolean retweetedByMe = resultSet.getBoolean("REWTWEETED");
        
        StatusData status = new StatusData(statusId, createdAt, text, favoriteCount, retweetCount, likedByMe, retweetedByMe);
        
        //Create new TweetModule
        return new TweetStatus(status, user);
        
    }

    /**
     * Used to validate if the user is already in the database (in the USERS table). 
     * 
     * @param userId of the user to validate
     * @return true if the user is already in the database. 
     * @throws SQLException 
     */
    @Override
    public boolean isUserAlreadyCreated(long userId) throws SQLException {
        
        LOG.info("Checking if " + userId + " exists");

        boolean result;
        
        String selectQuery = "SELECT * "
                            + "FROM USERS "
                            + "WHERE TWITTER_ID = ? "
                            + "LIMIT 1";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            pStatement.setLong(1, userId);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                
                //Checks if there is a result
                result = resultSet.next();
            
            }
        }
        
        LOG.info("Result is " + result);
        return result;   
        
    }

    /**
     * Used to know if an author of a tweet is already in the database. 
     * For now it just checks if the user id is found in the database. 
     * 
     * @param userData to validate is in the TWEET_AUTHORS table. 
     * @return true if the user is already in the database. 
     * @throws SQLException 
     */
    @Override
    public boolean isAuthorAlreadyCreated(UserData userData) throws SQLException {
        
        LOG.info("Checking if " + userData + " exists");

        boolean result;
        
        String selectQuery = "SELECT * "
                            + "FROM TWEET_AUTHORS "
                            + "WHERE TWITTER_ID = ? "
                            + "LIMIT 1";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            pStatement.setLong(1, userData.getId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                
                //Checks if there is a result
                result = resultSet.next();
            
            }
        }
        
        LOG.info("Result is " + result);
        return result;   
        
    }
    
    /**
     * Used to validate if the the tweet is already stored in the database. 
     * If it is already in the TWEETS table. 
     * 
     * @param statusData holding data about the tweet. 
     * @return true if the tweet is already in the tweets table. 
     * @throws SQLException 
     */
    @Override
    public boolean isTweetAlreadyCreated(StatusData statusData) throws SQLException {
        
        LOG.info("Checking if " + statusData + " exists");

        boolean result;
        
        String selectQuery = "SELECT * "
                            + "FROM TWEETS "
                            + "WHERE TWEET_ID = ? "
                            + "LIMIT 1";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            pStatement.setLong(1, statusData.getId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                
                //Checks if there is a result
                result = resultSet.next();
            
            }
        }
        
        LOG.info("Result is " + result);
        return result;  
        
    }

    /**
     * Used to update the record in the tweets table to the values in the given status data. 
     * 
     * @param statusData holding the new data to update the same status
     * @return the number of rows affected (should be 1). 
     * @throws SQLException 
     */
    @Override
    public int updateStatus(StatusData statusData) throws SQLException {
        
        LOG.info("Trying to update " + statusData);
        
        int result;

        String updateQuery = "UPDATE TWEETS SET CREATED_AT=?, TEXT=?, FAV_COUNT=?, RETWEET_COUNT=? WHERE TWEET_ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            
            //Solution from https://stackoverflow.com/questions/2400955/how-to-store-java-date-to-mysql-datetime
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(statusData.getCreatedAt());
            
            ps.setString(1, currentTime);
            ps.setString(2, statusData.getText());
            ps.setInt(3, statusData.getFavoriteCount());
            ps.setInt(4, statusData.getRetweetCount());
            ps.setLong(5, statusData.getId());
            
            result = ps.executeUpdate();
        }
        
        LOG.info("# of records updated : " + result);
        return result;
        
    }

    /**
     * Used to delete a tweet and user relationship from the database. 
     * This assumes both the user and tweet are already stored in the database. 
     * 
     * @param userId of the logged in user. 
     * @param tweetId of the status
     * @return number of rows affected (should be 1)
     * @throws SQLException 
     */
    @Override
    public int deleteUserTweetsRelationship(long userId, long tweetId) throws SQLException {

        LOG.info("Attempting to delete userId " + userId + " saved tweet " + tweetId + " from the TWEETS_USERS table");
        
        int result;

        String deleteQuery = "DELETE FROM TWEETS_USERS WHERE TWEET_ID = ? AND TWITTER_ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            
            ps.setLong(1, tweetId);
            ps.setLong(2, userId);
            
            result = ps.executeUpdate();
        
        }
        
        LOG.info("# of records deleted : " + result);
        return result;    
    
    }

    /**
     * Used to know if a user that is logged in has already saved a tweet to the database. 
     * 
     * @param userId of the logged in user
     * @param tweetId to be checked 
     * @return true if it is already saved and false otherwise. 
     * @throws SQLException 
     */
    @Override
    public boolean isTweetAlreadySavedByUser(long userId, long tweetId) throws SQLException {
        
        LOG.info("Checking if " + userId + " has already saved tweet id " + tweetId);

            boolean result;

            String selectQuery = "SELECT * "
                                + "FROM TWEETS_USERS "
                                + "WHERE TWEET_ID = ? AND TWITTER_ID = ? "
                                + "LIMIT 1";

            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

                pStatement.setLong(1, tweetId);
                pStatement.setLong(2, userId);

                try (ResultSet resultSet = pStatement.executeQuery()) {

                    //Checks if there is a result
                    result = resultSet.next();

                }
            }

            LOG.info("Result is " + result);
            return result;      
    
    }
    
}
