
package com.jeffreyboisvert.twitteratdawson.persistence;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import java.sql.SQLException;
import java.util.List;

/**
 * Used to interact with a database used to store data about tweets and users. 
 * 
 * @author Jeffrey Boisvert
 */
public interface TwitterTweetsDAO {

    // Create
    public int createStatus(StatusData statusData, long userId) throws SQLException;
    public int createTweetAuthor(UserData userData) throws SQLException;
    public int createUser(long twitterId) throws SQLException;
    public int createUserTweetRelationship(StatusData statusData, long userId) throws SQLException;

    // Read
    public List<TweetModule> findAllSavedStatusOfUser(long userId) throws SQLException;

    public boolean isUserAlreadyCreated(long userId) throws SQLException;
    public boolean isAuthorAlreadyCreated(UserData userData) throws SQLException;
    public boolean isTweetAlreadyCreated(StatusData statusData) throws SQLException;
    public boolean isTweetAlreadySavedByUser(long userId, long tweetId) throws SQLException;

    // Update
    public int updateStatus(StatusData statusData) throws SQLException;

    // Delete
    public int deleteUserTweetsRelationship(long userId, long tweetId) throws SQLException;

}
