package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.bean.Twiter4jProperties;
import com.jeffreyboisvert.twitteratdawson.business.CleanProperties;
import com.jeffreyboisvert.twitteratdawson.business.PropertiesManager;
import com.jeffreyboisvert.twitteratdawson.business.Twitter4jPropertiesFileValidator;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.util.Properties;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

/**
 * 
 * Controller used to interact with the form in charge of updating the twitter4j.properties file
 * 
 * @author Jeffrey Boisvert
 */
public class ChangeTwitter4jPropertiesMenuFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ChangeTwitter4jPropertiesMenuFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="dialogWindow"
    private DialogPane dialogWindow; // Value injected by FXMLLoader

    @FXML // fx:id="consumerKeyTextField"
    private TextField consumerKeyTextField; // Value injected by FXMLLoader

    @FXML // fx:id="consumerKeySecretTextField"
    private TextField consumerKeySecretTextField; // Value injected by FXMLLoader

    @FXML // fx:id="accessTokenTextField"
    private TextField accessTokenTextField; // Value injected by FXMLLoader

    @FXML // fx:id="accessTokenSecretTextField"
    private TextField accessTokenSecretTextField; // Value injected by FXMLLoader

    @FXML // fx:id="exitButton"
    private Button exitButton; // Value injected by FXMLLoader

    private final Twiter4jProperties twitter4jProperties;

    private final Twitter4jPropertiesFileValidator validator;

    private DialogMaker dialogMaker;

    /**
     * Default constructor to initialize instance variables not injected by FXML
     */
    public ChangeTwitter4jPropertiesMenuFXMLController() {

        this.validator = new Twitter4jPropertiesFileValidator(TwitterAtDawsonProjectConstants.PATH_TO_TWITTER4J_PROPERTIES);
        this.twitter4jProperties = new Twiter4jProperties();

    }

    /**
     * 
     * Simply clears the text fields of the form. 
     * 
     * @param event 
     */
    @FXML
    public void clearForm(ActionEvent event) {

        this.twitter4jProperties.setConsumerKey("");
        this.twitter4jProperties.setConsumerSecret("");
        this.twitter4jProperties.setAccessToken("");
        this.twitter4jProperties.setAccessTokenSecret("");

    }

    /**
     * Exit event handler. Used to close the dialog window
     * 
     * @param event 
     */
    @FXML
    public void exitWindow(ActionEvent event) {

        final Stage stage = (Stage) this.dialogWindow.getScene().getWindow();

        stage.close();

    }

    /**
     * 
     * Saves the form values to the twitter4j.properties file if it is not empty. 
     * If save is successful the window is closed.
     * 
     * @param event 
     */
    @FXML
    public void saveForm(ActionEvent event) {

        //If any of the form inputs are empty display alert and exit method
        if (areAnyOfFormInputsNotEmpty()) {

            this.dialogMaker.displayErrorAlert("Empty_Fields_Error");

            LOG.info("Unable to twitter4j properties due to empty fields");

            return;

        }

        if (areKeysInvalidTwitterDeveloperKeys()) {

            this.dialogMaker.displayErrorAlert("Keys_Are_Invalid");

            LOG.info("Unable to save keys since keys are not valid.");

            return;

        }

        Properties properties = new CleanProperties();

        //Set properties file to be written
        properties.setProperty(this.validator.getConsumerKeyLabel(), this.twitter4jProperties.getConsumerKey().trim());
        properties.setProperty(this.validator.getConsumerSecretLabel(), this.twitter4jProperties.getConsumerSecret().trim());
        properties.setProperty(this.validator.getAccessToken(), this.twitter4jProperties.getAccessToken().trim());
        properties.setProperty(this.validator.getAccessTokenSecret(), this.twitter4jProperties.getAccessTokenSecret().trim());

        PropertiesManager propertiesManager = new PropertiesManager();

        try {

            propertiesManager.writeTextProperties(TwitterAtDawsonProjectConstants.PATH_TO_TWITTER4J_PROPERTIES, TwitterAtDawsonProjectConstants.TWITTER4J_PROPERTIES_FILE_NAME, properties);

            LOG.info("Saved twitter4j properties");

            this.dialogMaker.displayInfoAlert("Restart_To_See_Changes");

            this.exitWindow(event);

        } catch (IOException ex) {

            //Display error message and exit
            LOG.error("IOException saveForm error", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

    /**
     * Used to disable to make the exit button close the whole program instead of just the 
     * dialog window
     * 
     */
    public void changeExitButtonToCloseApplication() {

        this.exitButton.setOnAction(this::exitApp);

    }

    /**
     * 
     * Used to close the app.
     * 
     * @param event 
     */
    @FXML
    public void exitApp(ActionEvent event) {

        LOG.info("Closing app from ChangeTwitter4jPropertiesMenuFXMLController");

        //Close window
        this.exitWindow(event);

        //Exit program
        Platform.exit();

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     */
    @FXML
    public void initialize() {

        assertFXMLInjectionWasSuccessful();

        //If it exists load values
        setUpTwitter4jProperties(this.twitter4jProperties);

        this.dialogMaker = new DialogMaker(this.resources);

        //Bind
        Bindings.bindBidirectional(this.consumerKeyTextField.textProperty(), this.twitter4jProperties.consumerKeyProperty());
        Bindings.bindBidirectional(this.consumerKeySecretTextField.textProperty(), this.twitter4jProperties.consumerSecretProperty());
        Bindings.bindBidirectional(this.accessTokenTextField.textProperty(), this.twitter4jProperties.accessTokenProperty());
        Bindings.bindBidirectional(this.accessTokenSecretTextField.textProperty(), this.twitter4jProperties.accessTokenSecretProperty());

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert consumerKeyTextField != null: "fx:id=\"consumerKeyTextField\" was not injected: check your FXML file 'ChangeTwitter4jPropertiesMenuFXML.fxml'.";
        assert consumerKeySecretTextField != null: "fx:id=\"consumerKeySecretTextField\" was not injected: check your FXML file 'ChangeTwitter4jPropertiesMenuFXML.fxml'.";
        assert accessTokenTextField != null: "fx:id=\"accessTokenTextField\" was not injected: check your FXML file 'ChangeTwitter4jPropertiesMenuFXML.fxml'.";
        assert accessTokenSecretTextField != null: "fx:id=\"accessTokenSecretTextField\" was not injected: check your FXML file 'ChangeTwitter4jPropertiesMenuFXML.fxml'.";

    }

    /**
     * Used to validate if the keys create valid. 
     * Based on code found here http://twitter4j.org/en/configuration.html
     *
     * @return true if it is invalid false if it is valid
     */
    private boolean areKeysInvalidTwitterDeveloperKeys() {

        try {

            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.setDebugEnabled(true)
                .setOAuthConsumerKey(this.twitter4jProperties.getConsumerKey())
                .setOAuthConsumerSecret(this.twitter4jProperties.getConsumerSecret())
                .setOAuthAccessToken(this.twitter4jProperties.getAccessToken())
                .setOAuthAccessTokenSecret(this.twitter4jProperties.getAccessTokenSecret());
            TwitterFactory tf = new TwitterFactory(configurationBuilder.build());
            Twitter twitter = tf.getInstance();

            User user = twitter.verifyCredentials();

            LOG.debug("Keys are valid");

            return false;

        } catch (TwitterException ex) {

            LOG.debug("Invalid keys", ex);

            return true;

        }


    }

    /**
     * 
     * Sets Twitter4jProperties object to values if the file already exists.
     * 
     * @param givenTwitter4jProperties 
     */
    private void setUpTwitter4jProperties(final Twiter4jProperties givenTwitter4jProperties) {

        PropertiesManager propertiesManager = new PropertiesManager();

        try {

            if (this.validator.isTwitter4jPropertiesPresent()) {

                Properties properties = propertiesManager.loadTextProperties(TwitterAtDawsonProjectConstants.PATH_TO_TWITTER4J_PROPERTIES, TwitterAtDawsonProjectConstants.TWITTER4J_PROPERTIES_FILE_NAME);

                givenTwitter4jProperties.setConsumerKey(properties.getProperty(this.validator.getConsumerKeyLabel()));
                givenTwitter4jProperties.setConsumerSecret(properties.getProperty(this.validator.getConsumerSecretLabel()));
                givenTwitter4jProperties.setAccessToken(properties.getProperty(this.validator.getAccessToken()));
                givenTwitter4jProperties.setAccessTokenSecret(properties.getProperty(this.validator.getAccessTokenSecret()));

            }
        } catch (IOException ex) {

            //Display error message and exit
            this.dialogMaker.displayErrorAlert("Major_Issue_Closing_App");
            LOG.error("IOException setUpTwitter4jProperties error", ex);
            Platform.exit();

        }
    }

    /**
     * 
     * Checks if the text fields of the form are empty
     * 
     * @return true if the fields (aka the Twitter4jProperties's fields bounded with the text fields) are empty and false otherwise
     */
    private boolean areAnyOfFormInputsNotEmpty() {

        TextValidator textValidator = new TextValidator();

        return !(textValidator.isStringNotSpaceOrBlank(this.twitter4jProperties.getConsumerKey()) &&
            textValidator.isStringNotSpaceOrBlank(this.twitter4jProperties.getConsumerSecret()) &&
            textValidator.isStringNotSpaceOrBlank(this.twitter4jProperties.getAccessToken()) &&
            textValidator.isStringNotSpaceOrBlank(this.twitter4jProperties.getAccessTokenSecret()));

    }

}