package com.jeffreyboisvert.twitteratdawson.presentation;

import java.util.ResourceBundle;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * Used to hold simple dialog logic such as displaying error messages. 
 * It also holds a resource which is used to make the messages internationalized. 
 * 
 * @author Jeffrey Boisvert
 */
public class DialogMaker {

    private final ResourceBundle resources;

    public DialogMaker(ResourceBundle resources) {

        this.resources = resources;

    }

    /**
     * Error message popup dialog
     *
     * @param messageBundleKey holds key value for string to set in content text field of dialog
     */
    public void displayErrorAlert(String messageBundleKey) {

        Alert dialog = new Alert(Alert.AlertType.ERROR);

        dialog.setTitle(resources.getString("Error"));

        dialog.setHeaderText(resources.getString("Error"));

        //Display main text and wrap it
        Text text = new Text(resources.getString(messageBundleKey));
        text.setWrappingWidth(300);
        text.setTextAlignment(TextAlignment.CENTER);
        dialog.getDialogPane().setContent(text);

        dialog.showAndWait();

    }

    /**
     * Creates a dialog pop up with the given DialogPane. 
     * 
     * @param pane holding reference to the DialogPane to show in a Dialog
     */
    public void displayDialogWithGivenDialogPane(DialogPane pane) {

        Dialog root = new Dialog();

        root.setDialogPane(pane);

        root.showAndWait();

    }

    /**
     * Creates a dialog pop up with the given node. 
     * 
     * @param node holding reference to a node to set in the Dialog.
     */
    public void displayDialogWithNode(Node node) {

        Dialog root = new Dialog();

        DialogPane dialogPane = new DialogPane();

        dialogPane.setContent(node);

        root.setDialogPane(dialogPane);

        //Testing
        root.getDialogPane().getButtonTypes().addAll(
            ButtonType.CLOSE
        );

        root.showAndWait();

    }

    /**
     * Info message popup dialog
     *
     * @param messageBundleKey holds key value for string to set in content text field of dialog
     */
    public void displayInfoAlert(String messageBundleKey) {

        Alert dialog = new Alert(Alert.AlertType.INFORMATION);

        dialog.setTitle(resources.getString("Info"));

        dialog.setHeaderText(resources.getString("Info"));

        //Display main text and wrap it
        Text text = new Text(resources.getString(messageBundleKey));
        text.setWrappingWidth(300);
        text.setTextAlignment(TextAlignment.CENTER);
        dialog.getDialogPane().setContent(text);

        dialog.showAndWait();

    }

}