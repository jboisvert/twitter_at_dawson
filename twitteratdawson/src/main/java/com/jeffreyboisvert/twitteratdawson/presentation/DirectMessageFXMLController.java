package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.bean.MessageProperty;
import com.jeffreyboisvert.twitteratdawson.business.SendDirectMessageApp;
import com.jeffreyboisvert.twitteratdawson.business.SendDirectMessageModule;
import com.jeffreyboisvert.twitteratdawson.business.TwitterUser;
import com.jeffreyboisvert.twitteratdawson.business.TwitterUserModule;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Controls the logic of displaying direct message module elements and holds references in module (such as buttons)
 * 
 * @author Jeffrey Boisvert
 */
public class DirectMessageFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(DirectMessageFXMLController.class);

    private final static int MAX_MESSAGE_SIZE = 140;

    private final static int MIN_MESSAGE_SIZE = 1;
    
    private final static int RECEIPT_HAS_NO_INDEX = -1;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML //fx:id="messagesListView
    private ListView < String > messagesListView; // Value injected by FXMLLoader

    @FXML // fx:id="usersList"
    private ListView < User > usersList; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageModule"
    private BorderPane directMessageModule; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageBorderPane"
    private BorderPane directMessageBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageText"
    private TextArea directMessageText; // Value injected by FXMLLoader

    @FXML // fx:id="sendDirectMessageButton"
    private Button sendDirectMessageButton; // Value injected by FXMLLoader
    
    private final MessageProperty directMessagePropertyBean;

    //Holding values TODO Make not string but messages
    private final ObservableList < String > items;

    private final ObservableList < User > users;

    //Holds reference to the user 
    private TwitterUserModule receipt;

    private DialogMaker dialogMaker;

    private final SendDirectMessageModule directMessageApp;

    /**
     * Default constructor to initialize objects not injected by the FXML loader
     */
    public DirectMessageFXMLController() {

        this.items = FXCollections.observableArrayList();
        this.directMessageApp = new SendDirectMessageApp();
        this.users = FXCollections.observableArrayList();
        this.directMessagePropertyBean = new MessageProperty(MIN_MESSAGE_SIZE, MAX_MESSAGE_SIZE);

    }

    /**
     * Used to load more messages into the Direct Message app 
     * @param event 
     */
    @FXML
    void loadMoreDirectMessagesFromTwitter(ActionEvent event) {

        try {

            this.directMessageApp.loadMoreMessages();

            //Update the conversation with new loaded messages if there is any
            updateConversation();

            //Notify the user that messages were refreshed so they hopefully do not spam it and give time
            //for messages to get to twitter to query
            this.dialogMaker.displayInfoAlert("Messages_Loaded");

        } catch (TwitterException ex) {

            LOG.error("Unable to load more messages", ex);

            //Notify the user that there was a problem
            this.dialogMaker.displayErrorAlert("Error_Sending_Direct_Message");

        }

    }

    /**
     * Set reference to receipt
     * 
     * @param receipt 
     */
    public void setUserReceipt(TwitterUserModule receipt) {

        this.receipt = receipt;
        
        //Used to ensure the receipt is selected in the listview
        int indexOfReceipt = getIndexOfReceipt();
        
        //Not a major issue to crash but user selected won't be highlighted.
        if(indexOfReceipt == RECEIPT_HAS_NO_INDEX){
            
            LOG.error("User was not found in list! screen name: " + this.receipt.getHandle() + " id: " + this.receipt.getId());
            
        }
        else {
            
           //Scroll and highlight selected user
           this.usersList.getSelectionModel().select(indexOfReceipt);
           this.usersList.scrollTo(indexOfReceipt);
           
        }
        
        enableMessages();

    }

    /**
     * Triggered when the user clicks on the button to send the direct message
     * 
     * @param event holds addition information about itself
     */
    @FXML
    public void sendDirectMessage(ActionEvent event) {

        if (this.directMessagePropertyBean.isMessageValid()) {

            try {

                DirectMessage messageSent = this.directMessageApp.sendMessage(this.receipt.getHandle(), this.directMessagePropertyBean.getMessage());

                //Update the list of messages
                this.items.add("You " + messageSent.getCreatedAt().toString() + ": " + messageSent.getText());

                scrollToBottomOfMessages();

                //Clearing text
                this.directMessagePropertyBean.setMessage("");

            } catch (TwitterException ex) {

                LOG.error("Direct Message: " + this.directMessagePropertyBean.getMessage() + " was not sent", ex);

                //Notify the user that there was a problem
                this.dialogMaker.displayErrorAlert("Error_Sending_Direct_Message");

            }

        } else {

            LOG.warn("sendDirectMessage invalid in DirectMessageFXMLController");

            dialogMaker.displayErrorAlert("Direct_Message_Blank");

        }

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assertFXMLInjectionWasSuccessful();

        this.dialogMaker = new DialogMaker(this.resources);

        Bindings.bindBidirectional(this.directMessageText.textProperty(), this.directMessagePropertyBean.getMessageProperty());

        setUserTiles();

        setMessageTiles();

        setReceiptsListView();

    }

    /**
     * Used to set the messages of a conversation 
     */
    private void setMessageTiles() {

        //Makes user tiles
        this.messagesListView.setCellFactory(p -> new MessageTileCell());

    }

    /**
     * Used to set the user tiles for users the user can message. 
     * This is also used to add a click event to set the conversation based on the selected user.
     */
    private void setUserTiles() {

        //Makes user tiles
        this.usersList.setCellFactory(p -> new UserTileCell());

        //Adding listeners for when a user is selected
        this.usersList.getSelectionModel().selectedItemProperty()
            .addListener((ObservableValue < ? extends User > ov, User previousUser, User currentUser) -> {

                //Just for debugging
                if (previousUser != null) {
                    LOG.debug("Previous selected user: " + previousUser.getScreenName());
                }

                //Take selected user and assign as receipt. 
                if (currentUser != null) {
                    LOG.debug("New receipt handle: " + currentUser.getScreenName());

                    this.receipt = new TwitterUser(currentUser);
                    
                    try {
                        
                        //Set to conversation is visible and usable
                        updateConversation();
                        
                    } catch (TwitterException ex) {
                        
                        LOG.error("Loading messages caused an error", ex);

                        //Notify the user that there was a problem
                        dialogMaker.displayErrorAlert("Error_Loading_Messages");
                        
                    }
                    enableMessages();

                }
            });

    }

    /**
     * Used scroll the messages list view to the bottom to show latest message. 
     */
    private void scrollToBottomOfMessages() {

        this.messagesListView.scrollTo(this.items.size() - 1);

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert directMessageBorderPane != null: "fx:id=\"directMessageBorderPane\" was not injected: check your FXML file 'DirectMessageFXML.fxml'.";
        assert directMessageText != null: "fx:id=\"directMessageText\" was not injected: check your FXML file 'DirectMessageFXML.fxml'.";
        assert sendDirectMessageButton != null: "fx:id=\"sendDirectMessageButton\" was not injected: check your FXML file 'DirectMessageFXML.fxml'.";

    }

    /**
     * Used to set the list of users the user can select to message.
     */
    private void setReceiptsListView() {

        try {

            List < User > usersFollowingUser = this.directMessageApp.getListOfReciptents();

            Collections.reverse(usersFollowingUser);
            
            LOG.debug("About to add users " + usersFollowingUser);

            //Adding the user objects to tiles
            usersFollowingUser.forEach((user) -> {
                LOG.info("Adding user ", user);
                this.users.add(user);
            });

        } catch (TwitterException ex) {

            LOG.error("loading receipts did not work", ex);

            //Notify the user that there was a problem
            dialogMaker.displayErrorAlert("Error_Loading_Messages");

        }

        this.usersList.setItems(users);

    }

    /**
     * Used to populate the messagesListView with the user's messages
     * @throws twitter4j.TwitterException when unable to load messages
     */
    public void updateConversation() throws TwitterException{

        LOG.info("updateConversation from twitter in DirectMessageFXMLController");

        //Clear what was there before
        this.items.clear();

        List < DirectMessage > messages = this.directMessageApp.getConversationWithAUser(this.receipt.getId());

        messages.forEach((message) -> {

                LOG.info("Got message: " + message.getText());

                //Checking if message is reciever or logged in user
                String messageSenderInfo = (message.getSenderId() == this.receipt.getId()) ? this.receipt.getHandle() : "You";
                this.items.add(messageSenderInfo + " " + message.getCreatedAt().toString() + " : " + message.getText());

            });

         scrollToBottomOfMessages();

        this.messagesListView.setItems(items);

    }

    /**
     * Used to re-enable the messages so the user can interact with it
     */
    private void enableMessages() {

        //Renable section
        this.directMessageModule.setDisable(false);

    }

    /**
     * Used to get the index of the set receipt. 
     * 
     * @return index of receipt. 
     */
    private int getIndexOfReceipt() {
        
        
        int indexOfReceipt = RECEIPT_HAS_NO_INDEX;
        
        for(int index = 0; index < this.users.size(); index++){
            
            if (this.users.get(index).getId() == (this.receipt.getId())){
                
                indexOfReceipt = index;
                break;
                
            }
        
        }
        
        return indexOfReceipt;
        
    }

}