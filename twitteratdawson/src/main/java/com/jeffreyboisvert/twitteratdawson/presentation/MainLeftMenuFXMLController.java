package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TwitterUserModule;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Controller for the main left. Used to change the content of the main border pane
 * given as reference. 
 * 
 * This class implements the singleton design pattern meaning only one can be in memory at all times. 
 * 
 * @author Jeffrey Boisvert
 */
public class MainLeftMenuFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainLeftMenuFXMLController.class);
    
    //Will be used to be able to get the singleton of the object
    private static MainLeftMenuFXMLController controllerSingleton;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="menuContainer"
    private VBox menuContainer; // Value injected by FXMLLoader
    
    @FXML // fx:id="directMessageButton"
    private Button directMessageButton; // Value injected by FXMLLoader

    //Holds the pane of the main contentto be loaded into
    private BorderPane mainContent;

    //Controls logic of which button is selected
    private final MenuElementHighlighter menuHighlighter;

    private DialogMaker dialogMaker;

    /**
     * Used to ensure the MainLeftMenu is only loaded once
     */
    public MainLeftMenuFXMLController() {
        
        //This is used to ensure there is only one main left menu at all times. 
        //Implementation of the singleton design pattern
        if (controllerSingleton == null) {
            
            //Set static variable to this
            controllerSingleton = this;
            
        } else {
            
            throw new RuntimeException("Singleton MainLeftMenuFXML. There cannot be more than 1 main menu!");
            
        }

        //Used to highlight the selected menu item
        ArrayList < String > buttonSelected = new ArrayList < > ();
        buttonSelected.add("button-selected");

        ArrayList < String > buttonUnselected = new ArrayList < > ();
        buttonUnselected.add("button");
        buttonUnselected.add("menu-item");

        this.menuHighlighter = new MenuElementHighlighter(buttonSelected, buttonUnselected);

    }

    /**
     * Used to exit the application
     * 
     * @param event 
     */
    @FXML
    public void exitApplication(ActionEvent event) {
        LOG.info("Exiting program");
        Platform.exit();
    }

    /**
     * Loads the direct message module into the main module
     * 
     * @param event 
     */
    @FXML
    public void openDirectMessage(ActionEvent event) {

        LOG.info("openDirectMessage from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_DIRECT_MESSAGE);

        highlightSelectedNodeFromActionEvent(event);

    }
    
    /**
     * Open the direct message module
     * 
     * @param user reference to the user to set the direct message to
     */
    public void openDirectMessageWithHandle(TwitterUserModule user){
        
        LOG.info("open direct messages with " + user.getHandle());
         
        loadDirectMessageModuleWithUser(TwitterAtDawsonProjectConstants.PATH_TO_FXML_DIRECT_MESSAGE, user);
        
        menuHighlighter.highlightButtonAsSelected(this.directMessageButton);
        
    }

    /**
     * Loads the user mentions module into the main module
     * 
     * @param event 
     */
    @FXML
    public void openMentions(ActionEvent event) {

        LOG.info("openMentions from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_TWEET_MENTIONS);

        highlightSelectedNodeFromActionEvent(event);

    }

    /**
     * Opens the retweets module into the main module
     * 
     * @param event 
     */
    @FXML
    public void openRetweets(ActionEvent event) {

        LOG.info("openRetweets from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_RETWEETS);

        highlightSelectedNodeFromActionEvent(event);

    }

    /**
     * Used to load the open search into the main module
     * 
     * @param event 
     */
    @FXML
    public void openSearch(ActionEvent event) {

        LOG.info("openSearch from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_SEARCH);

        highlightSelectedNodeFromActionEvent(event);

    }

    /**
     * Used to load the timeline module into the main module
     * 
     * @param event 
     */
    @FXML
    public void openTimeline(ActionEvent event) {

        LOG.info("openTimeline from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_TIMELINE_MODULE);

        highlightSelectedNodeFromActionEvent(event);

    }
    
    /**
     * Used to load the send tweet module.
     * 
     * @param event 
     */
    @FXML
    public void sendTweet(ActionEvent event) {

        LOG.info("sendTweet from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_SEND_TWEETS);

        highlightSelectedNodeFromActionEvent(event);
        
    }

    /**
     * Used to load the settings module into the main module
     * 
     * @param event 
     */
    @FXML
    public void openSettingsModule(ActionEvent event) {

        LOG.info("openSettingsModule from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_SETTINGS);

        highlightSelectedNodeFromActionEvent(event);

    }
    
    /**
     * Used to open the local timeline of saved tweets.
     * @param event 
     */
    @FXML
    public void openLocalTweetTimeline(ActionEvent event) {
        
        LOG.info("openLocalTweetTimeline from MainLeftMenuFXMLController");

        loadNewModule(TwitterAtDawsonProjectConstants.PATH_TO_SAVED_TWEETS_TIMELINE_MODULE_FXML);

        highlightSelectedNodeFromActionEvent(event);

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assert menuContainer != null: "fx:id=\"menuContainer\" was not injected: check your FXML file 'MainLeftMenuFXML.fxml'.";

        this.dialogMaker = new DialogMaker(this.resources);

    }


    /**
     * 
     * Used to set the mainContent that will be used to update the modules. 
     * 
     * @param pane reference to the main element content to be loading modules into
     */
    public void addMainPane(BorderPane pane) {

        this.mainContent = pane;

    }

    /**
     * Method dealing with the logic of loading an JavaFX Parent node from a FXML file
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node getElementFromFXMLLoader(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(MainLeftMenuFXMLController.class.getResource(path));

        //Give message bundles and other resources
        loader.setResources(this.resources);

        Node node = loader.load();

        return node;

    }
    
    /**
     * Sets the center of this.menuContainer to the FXML element loaded 
     * to the direct messages with a reference to a given user
     * 
     * @param path holds string to path of FXML module
     */
    private void loadDirectMessageModuleWithUser(String path, TwitterUserModule user) {

        try {

            //Load FXML from given path
            FXMLLoader loader = new FXMLLoader(MainLeftMenuFXMLController.class.getResource(path));

            //Give message bundles and other resources
            loader.setResources(this.resources);

            Node node = loader.load();

            DirectMessageFXMLController controller = loader.getController();
            
            controller.setUserReceipt(user);
            
            try {
                
                controller.updateConversation();
                
            } catch (TwitterException ex) {
                
                LOG.error("Loading messages caused an error", ex);

                 //Notify the user that there was a problem
                 dialogMaker.displayErrorAlert("Error_Loading_Messages");
            
            }

            this.mainContent.setCenter(node);

        } catch (IOException ex) {

            LOG.error("Error loading element from " + path, ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

    /**
     * 
     * Sets the center of this.menuContainer to the FXML element loaded 
     * from the given path. 
     * 
     * @param path holds string to path of FXML module
     */
    private void loadNewModule(String path) {

        try {

            //Get element
            Node element = getElementFromFXMLLoader(path);

            this.mainContent.setCenter(element);

        } catch (IOException ex) {

            LOG.error("Error loading element from " + path, ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

    /**
     * 
     * Used to get the node from the given ActionEvent and then highlight that Node if 
     * the event's source is of type Node if not issue is logged.
     * 
     * @param event holds reference of element that triggered event
     */
    private void highlightSelectedNodeFromActionEvent(ActionEvent event) {

        //Only highlight element if it is a Node
        if (event.getSource() instanceof Node) {

            menuHighlighter.highlightButtonAsSelected((Node) event.getSource());

        } else {

            LOG.warn("highlightSelectedNodeFromActionEvent parameter is not of type Node from MainLeftMenuFXMLController");

        }

    }

    /**
     * Gets the first element in the menu container and fires its click event
     */
    public void selectFirstMenuElement() {

        //Get first menu element
        Node node = menuContainer.getChildren().get(0);

        if (node instanceof Button) {

            Button firstMenuButton = (Button) node;

            //Simulate it being clicked
            firstMenuButton.fire();

        } else {

            LOG.warn("First element was not a button and event was not triggered in MainLeftMenuFXMLController");

        }

    }
    
    /**
     * Returns the single reference to the main left menu fxml controller. 
     * 
     * @return null if none is loaded in memory if not it returns the reference to the controller in memory.  
     */
    public static MainLeftMenuFXMLController getSingleton(){
        
        return controllerSingleton;
        
    }

}