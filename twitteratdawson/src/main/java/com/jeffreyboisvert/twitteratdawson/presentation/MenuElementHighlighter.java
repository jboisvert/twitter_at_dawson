package com.jeffreyboisvert.twitteratdawson.presentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.scene.Node;
import org.slf4j.LoggerFactory;

/**
 *
 * Used to keep track of last button clicked and to highlight 
 * that given button. This also handles reverting menu item button
 * back to previous state. This object is mutable.
 * 
 * @author Jeffrey Boisvert
 */
public class MenuElementHighlighter {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainLeftMenuFXMLController.class);

    //Holds reference to the current selected button
    private Node currentSelectedElement;
    private List<String> cssClassesWhenButtonSelect;
    private List<String> cssClassesWhenButtonUnselect;

    public MenuElementHighlighter(final List < String > cssClassesWhenButtonSelect, final List < String > cssClassesWhenButtonUnselect) {

        Objects.requireNonNull(cssClassesWhenButtonSelect, "cssClassesWhenButtonSelect must not be null");
        Objects.requireNonNull(cssClassesWhenButtonSelect, "cssClassesWhenButtonUnselect must not be null");

        this.cssClassesWhenButtonSelect = new ArrayList <> ();
        this.cssClassesWhenButtonUnselect = new ArrayList <> ();

        //Deep copy using functional approach
        cssClassesWhenButtonSelect.forEach((cssClass) -> {
            this.cssClassesWhenButtonSelect.add(cssClass);
        });

        //Deep copy using functional approach
        cssClassesWhenButtonUnselect.forEach((cssClass) -> {
            this.cssClassesWhenButtonUnselect.add(cssClass);
        });

    }


    /**
     * 
     * Used to highlight the newly selected button in the menu.
     * This method also disables the selected element
     * 
     * @param element is the given button element to set styling to
     * 
     */
    public void highlightButtonAsSelected(Node element) {

        Objects.requireNonNull(cssClassesWhenButtonSelect, "element must not be null");

        //If there was a previous button selected then unselect it
        this.unselectCurrentSelectedButton();

        element.getStyleClass().addAll(this.cssClassesWhenButtonSelect);
        element.setDisable(true);
        
        this.currentSelectedElement = element;

    }

    /**
     * Used to reset the style of the current selected button in menu.
     * This also re-enables the previous selected node
     */
    private void unselectCurrentSelectedButton() {

        if (this.currentSelectedElement != null) {

            this.currentSelectedElement.setDisable(false);
            this.currentSelectedElement.getStyleClass().clear();
            this.currentSelectedElement.getStyleClass().addAll(this.cssClassesWhenButtonUnselect);

        }

    }

}