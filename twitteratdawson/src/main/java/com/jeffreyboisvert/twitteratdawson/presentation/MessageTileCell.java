package com.jeffreyboisvert.twitteratdawson.presentation;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.text.Text;
import org.slf4j.LoggerFactory;

/**
 * Used to make a simple Text object that is used to add messages to the list view easily. 
 * 
 * @author Jeffrey Boisvert
 */
public class MessageTileCell extends ListCell < String > {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MessageTileCell.class);

    private final static int WRAPPING_TEXT_SIZE = 360;

    /**
     * This method is called when ever cells need to be updated
     *
     * @param message
     * @param empty
     */
    @Override
    protected void updateItem(String message, boolean empty) {

        super.updateItem(message, empty);

        LOG.info("updateItem in UserTile");

        if (empty || message == null) {

            setText(null);
            setGraphic(null);

        } else {


            LOG.info("Setting graphic for message tile");
            setGraphic(getUserTileGraphic(message));

        }
    }

    /**
     * Does logic so UI element encapsulates a message
     * 
     * @return UI Node to represent a MessageTile 
     */
    private Node getUserTileGraphic(String message) {

        Text text = new Text(message);
        text.setWrappingWidth(WRAPPING_TEXT_SIZE);

        return text;

    }

}