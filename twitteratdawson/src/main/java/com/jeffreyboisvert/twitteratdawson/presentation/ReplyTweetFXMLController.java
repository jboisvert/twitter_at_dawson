
package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.bean.MessageProperty;
import com.jeffreyboisvert.twitteratdawson.business.TweetContraints;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Used to control the UI elements needed to reply to a given tweet. 
 * 
 * @author Jeffrey Boisvert
 */
public class ReplyTweetFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ReplyTweetFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="sendReplyTextArea"
    private TextArea sendReplyTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="dialogWindow"
    private DialogPane dialogWindow; // Value injected by FXMLLoader

    private TweetModule tweet;

    private final MessageProperty replyMessageBean;

    private DialogMaker dialogMaker;

    /**
     * Default constructor
     */
    public ReplyTweetFXMLController() {

        this.replyMessageBean = new MessageProperty(TweetContraints.MIN_LENGTH, TweetContraints.MAX_LENGTH);

    }

    /**
     * Used to pass reference of a tweet to the reply controller. 
     * 
     * @param tweet 
     */
    public void setTweetModule(TweetModule tweet) {

        this.tweet = tweet;

    }

    /**
     * Used to reply to the given tweet module. 
     * 
     * @param event 
     */
    @FXML
    public void replyToTweet(ActionEvent event) {

        //Means previous element forget to set the tweet element. 
        //Tell user there was an error and close pane.
        if (this.tweet == null) {

            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);

            this.closeDialog(event);

        }

        if (this.replyMessageBean.isMessageValid()) {

            LOG.info("Sending tweet: " + this.replyMessageBean.getMessage());

            try {

                //Sending tweet
                this.tweet.replyToTweet(this.replyMessageBean.getMessage());

                //Clear text since tweet was sent
                this.replyMessageBean.setMessage("");

            } catch (TwitterException ex) {

                LOG.error("Tweet: " + this.replyMessageBean.getMessage() + " was not sent", ex);

                //Notify the user that there was a problem
                dialogMaker.displayErrorAlert("Error_Sending_Tweet");

            }

        } else {

            LOG.warn("tweet was not valid so not sending");

            //Notify the user the tweet is invalid
            dialogMaker.displayErrorAlert("Invalid_Tweet");

        }

    }

    /**
     * Used to close the dialog pane. 
     * This allows to close the dialog pane and not the whole application.
     * 
     * @param event 
     */
    @FXML
    public void closeDialog(ActionEvent event) {

        final Stage stage = (Stage) this.dialogWindow.getScene().getWindow();

        stage.close();

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    public void initialize() {

        assert sendReplyTextArea != null: "fx:id=\"sendReplyTextArea\" was not injected: check your FXML file 'ReplyTweetFXML.fxml'.";

        this.dialogMaker = new DialogMaker(this.resources);

        //Make binding with object
        Bindings.bindBidirectional(this.sendReplyTextArea.textProperty(), this.replyMessageBean.getMessageProperty());

    }


}