package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TweetsUserRetweetedTimelineApp;
import com.jeffreyboisvert.twitteratdawson.business.UserTweetsRetweetedTimelineApp;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import static com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants.PATH_TO_TIMELINE_LIST_VIEW_FXML;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import org.slf4j.LoggerFactory;
import javafx.scene.layout.BorderPane;

/**
 * 
 * This class is used to handle Retweets of various kinds (retweets by others 
 * aka who the user follows, retweets done by the user, user's tweets that were retweeted). 
 * It also handles tweets that mention the user as well based on what the user selects
 * 
 * @author Jeffrey Boisvert
 */
public class RetweetsFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="retweetsByYouButton"
    private Button retweetsByYouButton; // Value injected by FXMLLoader

    @FXML // fx:id="retweetModuleBorderPane"
    private BorderPane retweetModuleBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="yourTweetsRetweetedButton"
    private Button yourTweetsRetweetedButton; // Value injected by FXMLLoader    

    @FXML // fx:id="toggleButtonsContainer"
    private HBox toggleButtonsContainer; // Value injected by FXMLLoader 

    //Holds list of user's retweets
    private BorderPane userRetweets;

    //Controller for user's retweets
    private TimelineListViewController userRetweetsController;

    //Holds list of user's tweets that were retweeted
    private BorderPane userTweetsRetweeted;

    //Controller for user tweets retweeted. 
    private TimelineListViewController userTweetsRetweetedController;

    private final MenuElementHighlighter menuHighlighter;

    private DialogMaker dialogMaker;

    /**
     * Default constructor used to initialize any instance variables not injected by FXML loader
     */
    public RetweetsFXMLController() {

        //Used to highlight the selected menu item
        ArrayList < String > buttonSelected = new ArrayList < > ();
        buttonSelected.add("button-selected");

        ArrayList < String > buttonUnselected = new ArrayList < > ();
        buttonUnselected.add("button");
        buttonUnselected.add("menu-item");
        buttonUnselected.add("tab");

        this.menuHighlighter = new MenuElementHighlighter(buttonSelected, buttonUnselected);


    }

    /**
     * Used to load retweets by the user into the listing of tweets 
     * @param event 
     */
    @FXML
    public void loadRetweetsByYou(ActionEvent event) {

        menuHighlighter.highlightButtonAsSelected(this.retweetsByYouButton);

        loadRetweetsByUser();

    }

    /**
     * Used to load tweets that belonged to the user that were retweeted into the listing of tweets 
     * @param event 
     */
    @FXML
    public void loadTweetsRetweeted(ActionEvent event) {

        menuHighlighter.highlightButtonAsSelected(this.yourTweetsRetweetedButton);

        loadUserTweetsRetweeted();

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assertFXMLInjectionWasSuccessful();

        selectFirstMenuElement();

        this.dialogMaker = new DialogMaker(this.resources);

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert retweetsByYouButton != null: "fx:id=\"retweetsByYouButton\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";
        assert yourTweetsRetweetedButton != null: "fx:id=\"yourTweetsRetweetedButton\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";
        assert retweetModuleBorderPane != null: "fx:id=\"retweetModuleBorderPane\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";

    }

    /**
     * Gets the first element in the menu container and fires its click event
     */
    private void selectFirstMenuElement() {

        //Get first menu element
        Button firstMenuButton = (Button) this.toggleButtonsContainer.getChildren().get(0);

        //Simulate it being clicked
        firstMenuButton.fire();

    }

    /**
     * Used to load and set the main content to the list view of the tweets retweeted by the user.
     */
    private void loadRetweetsByUser() {

        //If null means need to intialize
        if (this.userRetweets == null || this.userRetweetsController == null) {

            try {

                setRetweetByUserTimelineElement(PATH_TO_TIMELINE_LIST_VIEW_FXML);

            } catch (IOException ex) {

                LOG.error("Error loading fxml in loadRetweetsByUser ", ex);
                this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
                Platform.exit();
            }

        }

        //Load and refresh
        this.userRetweetsController.loadAndRefreshTimeline();

        this.retweetModuleBorderPane.setCenter(this.userRetweets);


    }

    /**
     * Used to load and set the main content to the list view of tweets of user retweeted
     */
    private void loadUserTweetsRetweeted() {

        //If null means need to intialize
        if (this.userTweetsRetweeted == null || this.userTweetsRetweetedController == null) {

            try {

                setUserTweetsRetweetedTimelineElement(PATH_TO_TIMELINE_LIST_VIEW_FXML);

            } catch (IOException ex) {

                LOG.error("Error loading fxml in loadUserTweetsRetweeted ", ex);
                this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
                Platform.exit();
            }

        }

        //Load and refresh
        this.userTweetsRetweetedController.loadAndRefreshTimeline();

        this.retweetModuleBorderPane.setCenter(this.userTweetsRetweeted);

    }


    /**
     * Method dealing with the logic of setting the controller and loading the timeline UI element for tweets of user retweeted
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private void setUserTweetsRetweetedTimelineElement(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        this.userTweetsRetweeted = loader.load();

        //Get reference to the controller
        this.userTweetsRetweetedController = loader.getController();

        this.userTweetsRetweetedController.setTimelineModule(new UserTweetsRetweetedTimelineApp());

    }

    /**
     * Method dealing with the logic of setting the controller and loading the timeline UI element 
     * for the tweets retweeted by user.
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private void setRetweetByUserTimelineElement(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        this.userRetweets = loader.load();

        //Get reference to the controller
        this.userRetweetsController = loader.getController();

        this.userRetweetsController.setTimelineModule(new TweetsUserRetweetedTimelineApp());

    }

}