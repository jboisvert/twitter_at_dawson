package com.jeffreyboisvert.twitteratdawson.presentation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import javafx.stage.Stage;

/**
 * 
 * Just used as the skeleton of the javafx program. Holds blank elements.
 * It loads the wanted elements into the blank fields. 
 * 
 * @author Jeffrey Boisvert
 */
public class RootLayoutFXMLController {

    // Used for logging issues in RootLayout
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(RootLayoutFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mainBorderPane"
    private BorderPane mainBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="mainLeftArea"
    private VBox mainLeftArea; // Value injected by FXMLLoader

    private DialogMaker dialogMaker;

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    void initialize() {

        assertFXMLInjectionWasSuccessful();

        initializeModules();

        this.dialogMaker = new DialogMaker(this.resources);

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert mainLeftArea != null: "fx:id=\"mainLeftArea\" was not injected: check your FXML file 'RootLayoutFXML.fxml'.";
        assert mainBorderPane != null: "fx:id=\"mainBorderPane\" was not injected: check your FXML file 'RootLayoutFXML.fxml'.";

    }

    /**
     * Used to initialize modules of UI (left area, center area, etc). 
     */
    private void initializeModules() {
        initLeftArea();
    }

    /**
     * Used to initialize the left area of the RootLayout
     */
    private void initLeftArea() {

        try {

            FXMLLoader loader = new FXMLLoader(RootLayoutFXMLController.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_MAIN_LEFT_MENU));
            
            loader.setResources(this.resources);

            //Initialize menu
            Parent leftAreaModule = loader.load();

            mainLeftArea.getChildren().add(leftAreaModule);

            // Give the controller the data object.
            MainLeftMenuFXMLController mainLeftAreaController = loader.getController();

            //Do actions needed by controller
            mainLeftAreaController.addMainPane(this.mainBorderPane);
            mainLeftAreaController.selectFirstMenuElement();

        } catch (IOException ex) {

            //TODO Make logging more detailed
            LOG.error("initLeftArea error", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

}