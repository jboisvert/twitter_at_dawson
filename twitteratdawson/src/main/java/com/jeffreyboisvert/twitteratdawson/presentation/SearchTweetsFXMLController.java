package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.bean.MessageProperty;
import com.jeffreyboisvert.twitteratdawson.business.HomeTimelineApp;
import com.jeffreyboisvert.twitteratdawson.business.SearchTimelineApp;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import org.slf4j.LoggerFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

/**
 * Used to present how to search for tweets for the user on Twitter. 
 * 
 * @author Jeffrey Boisvert
 */
public class SearchTweetsFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SearchTweetsFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="searchModuleBorderPane"
    private BorderPane searchModuleBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="searchTextField"
    private TextField searchTextField; // Value injected by FXMLLoader

    @FXML // fx:id="searchButton"
    private Button searchButton; // Value injected by FXMLLoader

    private DialogMaker dialogMaker;
    
    private final MessageProperty searchProperty; 
    
    private TimelineListViewController timelineController;

    
    /**
     * Default constructor that intialized message property with default character limits.
     */
    public SearchTweetsFXMLController(){
        
        this.searchProperty = new MessageProperty();
        
    }
    
    /**
     * Used to conduct a search for tweets on Twitter based on the search term.
     * 
     * @param event 
     */
    @FXML
    public void searchTweetForTweetsMatchingString(ActionEvent event) {

        if (this.searchProperty.isMessageValid()) {
            
            loadTweets(this.searchProperty.getMessage());
            
            //Clear search term
            this.searchProperty.setMessage("");
            
        } else {
            
            LOG.warn("Search term is invalid! " + this.searchProperty.getMessage());
            this.dialogMaker.displayErrorAlert("Empty_Fields_Error");
        
        }

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assertFXMLInjectionWasSuccessful();

        this.dialogMaker = new DialogMaker(this.resources);
        
        //Make binding with object
        Bindings.bindBidirectional(this.searchTextField.textProperty(), this.searchProperty.getMessageProperty());

    }
    
    /**
     * Used to set the timeline list view which will be used to set the TwitterTimeineModule to be used in that controller.
     * This also gives reference to the controller to be later used. 
     */
    private void setTimelineListView(String query) {

        try {

            Node twitterModule = getTimelineElement(TwitterAtDawsonProjectConstants.PATH_TO_TIMELINE_LIST_VIEW_FXML, query);

            this.timelineController.loadAndRefreshTimeline();
            
            this.searchModuleBorderPane.setCenter(twitterModule);

        } catch (IOException ex) {

            LOG.error("Error loading fxml in setTimelineListView", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }
    }

    /**
     * Used to load tweets into the tweet list view that match the search term
     * 
     * @param searchTerm 
     */
    private void loadTweets(String searchTerm) {

        LOG.info("Loading tweets matching search: " + searchTerm);
                
        setTimelineListView(searchTerm);

    }
    
    /**
     * Method dealing with the logic of setting the controller and loading the timeline UI element
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node getTimelineElement(String path, String query) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        Node node = loader.load();

        //Get reference to the controller
        this.timelineController = loader.getController();

        //Set the controller to be using the home timeline app. 
        this.timelineController.setTimelineModule(new SearchTimelineApp(query));

        return node;

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert searchModuleBorderPane != null : "fx:id=\"searchModuleBorderPane\" was not injected: check your FXML file 'SearchTweetsFXML.fxml'.";
        assert searchTextField != null : "fx:id=\"searchTextField\" was not injected: check your FXML file 'SearchTweetsFXML.fxml'.";
        assert searchButton != null : "fx:id=\"searchButton\" was not injected: check your FXML file 'SearchTweetsFXML.fxml'.";

    }

}