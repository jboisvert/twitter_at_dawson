
package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.bean.MessageProperty;
import com.jeffreyboisvert.twitteratdawson.business.SendTweetApp;
import com.jeffreyboisvert.twitteratdawson.business.SendTweetModule;
import com.jeffreyboisvert.twitteratdawson.business.TweetContraints;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Used to hold the UI/UX for a user to send a tweet.
 * 
 * @author Jeffrey Boisvert
 */
public class SendTweetFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SendTweetFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="sendTweetArea"
    private BorderPane sendTweetArea; // Value injected by FXMLLoader

    @FXML // fx:id="sendTweetTextArea"
    private TextArea sendTweetTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="sendTweetButton"
    private Button sendTweetButton; // Value injected by FXMLLoader

    private DialogMaker dialogMaker;

    private SendTweetModule sendTweetModule;

    private final MessageProperty tweetMessage;

    private String retweetURL;

    /**
     * Default constructor
     */
    public SendTweetFXMLController() {

        this.tweetMessage = new MessageProperty(TweetContraints.MIN_LENGTH, TweetContraints.MAX_LENGTH);

        //By default it is blank
        this.retweetURL = "";

    }

    @FXML
    void sendTweet(ActionEvent event) {

        if (isTweetValid()) {

            LOG.info("Sending tweet: " + this.tweetMessage.getMessage());

            try {

                //Append retweet url if it exists. 
                this.tweetMessage.appendStringToMessage(retweetURL);

                //Sending tweet (trim removes any unwanted white space (also means if the URL is blank the extra space is removed)
                //And this is okay because after this it is set to a blank. 
                this.sendTweetModule.sendTweet(this.tweetMessage.getMessage().trim());

                //Clear text since tweet was sent
                this.tweetMessage.setMessage("");

            } catch (TwitterException ex) {

                LOG.error("Tweet: " + this.tweetMessage.getMessage() + " was not sent", ex);

                //Notify the user that there was a problem
                dialogMaker.displayErrorAlert("Error_Sending_Tweet");

            }

        } else {

            LOG.warn("tweet was not valid so not sending");

            //Notify the user the tweet is invalid
            dialogMaker.displayErrorAlert("Invalid_Tweet");

        }

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert sendTweetArea != null: "fx:id=\"sendTweetArea\" was not injected: check your FXML file 'SendTweetFXML.fxml'.";
        assert sendTweetTextArea != null: "fx:id=\"sendTweetTextArea\" was not injected: check your FXML file 'SendTweetFXML.fxml'.";
        assert sendTweetButton != null: "fx:id=\"sendTweetButton\" was not injected: check your FXML file 'SendTweetFXML.fxml'.";

        this.dialogMaker = new DialogMaker(resources);

        this.sendTweetModule = new SendTweetApp();

        //Make binding with object
        Bindings.bindBidirectional(this.sendTweetTextArea.textProperty(), this.tweetMessage.getMessageProperty());


    }

    /**
     * Used to allow already constructed tweets to be sent and the user can see them. 
     * 
     * @param givenMessage to assign 
     */
    public void setTweetMessageProperty(String givenMessage) {

        Objects.requireNonNull(givenMessage);

        this.tweetMessage.setMessage(givenMessage);

    }

    /**
     * Used to set the retweet url if there is one. 
     * If set this changes the limit of the tweet message property. 
     * 
     * @param URL of the retweet.  
     */
    public void setRetweetURL(String URL) {

        this.retweetURL = URL;

        //Calculate the new limit (remove an extra 1 to include the space between message and URL)
        int newMaxLimit = (TweetContraints.MAX_LENGTH - URL.length()) - 1;

        //Set new limit. 
        this.tweetMessage.setMaxMessageSize(newMaxLimit);

    }

    /**
     * Checks if the tweet has any white space or is empty
     * or is not within the bounds specified in the MessageProperty bean
     * 
     * @return true if valid false otherwise 
     */
    private boolean isTweetValid() {

        return this.tweetMessage.isMessageValid();

    }

}