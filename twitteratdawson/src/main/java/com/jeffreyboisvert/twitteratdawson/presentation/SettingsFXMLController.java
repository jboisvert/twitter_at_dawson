package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.MainApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import org.slf4j.LoggerFactory;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import javafx.application.Platform;

/**
 * 
 * Used to hold the UI for any settings the user may need
 * 
 * @author Jeffrey Boisvert
 */
public class SettingsFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SearchTweetsFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    private DialogMaker dialogMaker;

    /**
     * 
     * Used to open a DialogPane holding the twitter4j.properties form
     * 
     * @param event 
     */
    @FXML
    public void onTwitter4jPropertiesButtonClick(ActionEvent event) {

        LOG.info("onTwitter4jPropertiesButtonClick in SettingsFXMLController");

        //Load Twitter4j form dialogpane
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_TWITTER4J_DIALOG));

        loader.setResources(resources);

        try {

            Parent root = loader.load();

            if (root instanceof DialogPane) {

                Dialog dialog = new Dialog();

                dialog.setDialogPane((DialogPane) root);

                //Show the menu and wait until the user is done with it
                dialog.showAndWait();

            } else {

                LOG.warn("root was not an instance of DialogPane");

            }

        } catch (IOException | IllegalStateException ex) {

            LOG.error("Error loading fxml in onTwitter4jPropertiesButtonClick in SettingsFXMLController", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }
    
    

    /**
     * Used to open the help dialog window.
     * 
     * @param event 
     */
    @FXML
    public void onHelpButtonClicked(ActionEvent event) {
        
        LOG.info("onHelpButtonClicked in SettingsFXMLController");

        //Load Twitter4j form dialogpane
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_HELP_PAGE));

        loader.setResources(resources);

        try {

            Parent root = loader.load();

            if (root instanceof DialogPane) {

                dialogMaker.displayDialogWithNode((DialogPane) root);

            } else {

                LOG.warn("root was not an instance of DialogPane");

            }

        } catch (IOException | IllegalStateException ex) {

            LOG.error("Error loading fxml in onTwitter4jPropertiesButtonClick in SettingsFXMLController", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        this.dialogMaker = new DialogMaker(this.resources);

    }
}