package com.jeffreyboisvert.twitteratdawson.presentation;

import org.slf4j.LoggerFactory;

/**
 * Holds different methods to validate text (Strings)
 * 
 * @author Jeffrey Boisvert
 */
public class TextValidator {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TextValidator.class);

    /**
     * Validates the given string is not length of 0, null or white space
     * 
     * @param text holding the text to test
     * @return true if valid false otherwise
     */
    public boolean isStringNotSpaceOrBlank(final String text) {

        if (text == null) {
            return false;
        }

        //Remove white space
        String trimmedText = text.trim();

        return trimmedText.length() != 0;

    }

    /**
     * 
     * Checks if the first given character of the string matching the given character
     * 
     * @param text to check 
     * @param firstCharacter to see if first character in string
     * @return 
     */
    public boolean isFirstCharacterMatchingGivenCharacter(String text, char firstCharacter) {

        return text.charAt(0) == firstCharacter;

    }

    /**
     * Checks if there are any characters besides the first character
     * 
     * @param text to check 
     * @return true if there are characters following the first character
     * 
     */
    public boolean isThereCharactersAfterFirstCharacter(String text) {

        return text.length() > 1;

    }

    /**
     * Used to check if a text is within the bounds specified.
     * 
     * @param text to check 
     * @param lowerBound number (inclusive)
     * @param upperBound number (inclusive)
     * @return 
     */
    public boolean isTextLengthWithinBounds(String text, int lowerBound, int upperBound) {

        return text.length() >= lowerBound && text.length() <= upperBound;

    }

}