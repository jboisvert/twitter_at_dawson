package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.business.TwitterTimelineModule;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class is used to encapsulate the behaviour of a Twitter timeline.
 * This class is flexible to deal with different timeline as long as they load
 * tweets aka TweetModules. 
 * 
 * @author Jeffrey Boisvert
 */
public class TimelineListViewController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TimelineListViewController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="timelineListView"
    private ListView < TweetModule > timelineListView; // Value injected by FXMLLoader

    private final ObservableList < TweetModule > timelineObservableList;

    private TwitterTimelineModule timelineModule;
    
    @FXML // fx:id="loadMoreSection"
    private HBox loadMoreSection; // Value injected by FXMLLoader

    private DialogMaker dialogMaker;

    /**
     * Default constructor used to initialize basic variables. 
     */
    public TimelineListViewController() {

        this.timelineObservableList = FXCollections.observableArrayList();

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert timelineListView != null: "fx:id=\"timelineListView\" was not injected: check your FXML file 'TimelineListView.fxml'.";

        this.dialogMaker = new DialogMaker(this.resources);

        setTimelineTweetTiles();

    }
    
    /**
     * Used to load more status objects
     * 
     * @param event 
     */
    @FXML
    public void loadMoreStatus(ActionEvent event) {
        
        loadAndRefreshTimeline();
    
    }

    /**
     * Used to set the logic on how the timeline will be used.
     * 
     * @param timelineModule 
     */
    public void setTimelineModule(TwitterTimelineModule timelineModule) {

        this.timelineModule = timelineModule;

    }

    /**
     * Used to load and refresh the timeline of tweets based on the timeline module given.
     */
    public void loadAndRefreshTimeline() {

        try {

            //Loads more tweets
            this.timelineModule.loadTimeline();

            List < TweetModule > timelineTweets = this.timelineModule.getTimeline();

            LOG.debug("About to add timeline tweets " + timelineTweets);

            //Adding the user objects to tiles
            timelineTweets.forEach((tweet) -> {
                
                if(!this.timelineObservableList.contains(tweet)){
                    LOG.info("Adding tweet ", tweet);
                    this.timelineObservableList.add(tweet);
                }

                
            });

        } catch (TwitterException ex) {

            LOG.error("loading receipts did not work", ex);

            //Notify the user that there was a problem
            dialogMaker.displayErrorAlert("Error_Loading_Timeline");

        }

        this.timelineListView.setItems(this.timelineObservableList);

    }

    /**
     * Used to set the tweet tiles in the timeline. 
     * This is also used to add a click event to allow the user to be able to view more details 
     * of a given tweet. 
     */
    private void setTimelineTweetTiles() {

        //Makes user tiles
        this.timelineListView.setCellFactory(p -> new TweetTileCell(this.resources));

    }
    
    /**
     * Used to remove the load more button.
     */
    public void removeLoadMoreSection(){
        
        this.loadMoreSection.visibleProperty().set(false);
        
    }

}