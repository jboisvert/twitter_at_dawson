package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.HomeTimelineApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import org.slf4j.LoggerFactory;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Node;

/**
 * 
 * Module used to send tweets and see a timeline acting as a news feed. 
 * 
 * @author Jeffrey Boisvert
 */
public class TimelineTwitterModuleFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TimelineTwitterModuleFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="timelineModule"
    private BorderPane timelineModule; // Value injected by FXMLLoader

    @FXML // fx:id="timelineArea"
    private BorderPane timelineArea; // Value injected by FXMLLoader

    private TimelineListViewController timelineController;

    private DialogMaker dialogMaker;

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assertFXMLInjectionWasSuccessful();

        setSendTweetsModule();

        setTimelineListView();

        loadTimeline();

        this.dialogMaker = new DialogMaker(this.resources);

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert timelineModule != null: "fx:id=\"timelineModule\" was not injected: check your FXML file 'TimelineTwitterModuleFXML.fxml'.";
        assert timelineArea != null: "fx:id=\"timelineArea\" was not injected: check your FXML file 'TimelineTwitterModuleFXML.fxml'.";

    }

    /**
     * Used to load all the tweets of the users timeline and adding them to the timeline area
     */
    private void loadTimeline() {

        LOG.info("Loading home timeline");

        this.timelineController.loadAndRefreshTimeline();

    }

    /**
     * Method dealing with the logic of loading an JavaFX Parent node from a FXML file
     * May use in future TODO: Decide if needed
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node getElementFromFXMLLoader(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        Node node = loader.load();

        return node;

    }

    /**
     * Method dealing with the logic of setting the controller and loading the timeline UI element
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node getTimelineElement(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        Node node = loader.load();

        //Get reference to the controller
        this.timelineController = loader.getController();

        //Set the controller to be using the home timeline app. 
        this.timelineController.setTimelineModule(new HomeTimelineApp());

        return node;

    }

    /**
     * Used to set the send tweets module at the bottom of the border pane.
     */
    private void setSendTweetsModule() {

        try {

            Node twitterModule = getElementFromFXMLLoader(TwitterAtDawsonProjectConstants.PATH_TO_FXML_SEND_TWEETS);

            this.timelineModule.setBottom(twitterModule);

        } catch (IOException ex) {

            LOG.error("Error loading fxml in setSendTweetsModule in TimelineTwitterModuleFXMLController", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }


    }

    /**
     * Used to set the timeline list view which will be used to set the TwitterTimeineModule to be used in that controller.
     * This also gives reference to the controller to be later used. 
     */
    private void setTimelineListView() {

        try {

            Node twitterModule = getTimelineElement(TwitterAtDawsonProjectConstants.PATH_TO_TIMELINE_LIST_VIEW_FXML);

            this.timelineArea.setCenter(twitterModule);

        } catch (IOException ex) {

            LOG.error("Error loading fxml in setSendTweetsModule in TimelineTwitterModuleFXMLController", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }
    }

}