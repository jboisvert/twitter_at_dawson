package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TweetDAOManager;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.business.TwitterUser;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.Period;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Controller for creating a tweet FXML element. The fields different elements 
 * can be changed. This Object is mutable. 
 * 
 * @author Jeffrey Boisvert
 */
public class TweetFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tweetBorderPane"
    private BorderPane tweetBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="profileImageView"
    private ImageView profileImageView; // Value injected by FXMLLoader

    @FXML // fx:id="twitterNameLabel"
    private Label twitterNameLabel; // Value injected by FXMLLoader

    @FXML // fx:id="twitterUserHandle"
    private Label twitterUserHandle; // Value injected by FXMLLoader

    @FXML // fx:id="tweetDateLabel"
    private Label tweetDateLabel; // Value injected by FXMLLoader

    @FXML // fx:id="tweetMessage"
    private Text tweetMessage; // Value injected by FXMLLoader

    @FXML // fx:id="numberOfCommentsLabel"
    private Label numberOfCommentsLabel; // Value injected by FXMLLoader

    @FXML // fx:id="numberOfRetweetsLabel"
    private Label numberOfRetweetsLabel; // Value injected by FXMLLoader

    @FXML // fx:id="likeButton"
    private Button likeButton; // Value injected by FXMLLoader

    @FXML // fx:id="retweetWithNoMessageButton"
    private Button retweetWithNoMessageButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="saveTweetButton"
    private Button saveTweetButton; // Value injected by FXMLLoader

    @FXML // fx:id="likeButtonImage"
    private ImageView likeButtonImage; // Value injected by FXMLLoader

    @FXML // fx:id="numberOfLikesLabel"
    private Label numberOfLikesLabel; // Value injected by FXMLLoader

    private TweetModule tweet;

    private DialogMaker dialogMaker;

    /**
     * Used to like a tweet. 
     * The current implementation only allows to like then disable the button. 
     * Specs also specify only needing to like so for now button is disabled
     *
     * @param event 
     */
    @FXML
    public void likeTweet(ActionEvent event) {

        if (this.tweet == null) {

            LOG.error("User is attempting to like a tweet but the Tweet is null.");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        LOG.info("Attempting to like tweet");
        try {

            this.tweet.likeTweet();

            updateUIElementsToReflectTweet();


        } catch (TwitterException ex) {

            LOG.error("Unable to like tweet with id " + this.tweet.getTweetId(), ex);

            this.dialogMaker.displayErrorAlert("Unable_To_Like_Tweet");

        }

        //Do not allow the user to like the tweet anymore
        this.setActionToUnlike();

    }
    
    /**
     * Used to unlike the tweet. 
     * 
     * @param event 
     */
    public void unlikeTweet(ActionEvent event){
        
        if (this.tweet == null) {

            LOG.error("User is attempting to unlike a tweet but the Tweet is null.");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        LOG.info("Attempting to unlike tweet");
        try {

            this.tweet.unlikeTweet();
            
            updateUIElementsToReflectTweet();


        } catch (TwitterException ex) {

            LOG.error("Unable to unlike tweet with id " + this.tweet.getTweetId(), ex);

            this.dialogMaker.displayErrorAlert("Unable_To_UnLike_Tweet");

        }

        //Make it so the user can like the tweet again
        this.setActionToLike();
        
    }
    
    /**
     * Used to set the like button to unlike action button
     */
    private void setActionToUnlike(){
        
        //Make the button now unlike the tweet
        this.likeButton.setOnAction(this::unlikeTweet);
        
        //Add style to indicate it is liked
        this.likeButton.getStyleClass().addAll("tweet-like");

    }
    
    /**
     * Used to set the unlike button to a like action button.
     */
    private void setActionToLike(){
        
        //Make the button now like the tweet
        this.likeButton.setOnAction(this::likeTweet);
        
        //Reset style back to normal
        this.likeButton.getStyleClass().clear();
        this.likeButton.getStyleClass().addAll("button", "tweet-control-button");
        
    }

    /**
     * Used to open a dialog pane with a reply form. 
     * It gives it reference to this tweet. 
     * 
     * @param event 
     */
    @FXML
    public void replyToTweet(ActionEvent event) {

        LOG.info("Attempting to reply to tweet");

        if (this.tweet == null) {

            LOG.error("Attempting to reply to this tweet but the tweet object is null");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        try {

            DialogPane replyModule = getReplyDialogPane();

            //Automatically shows and prompts user
            this.dialogMaker.displayDialogWithGivenDialogPane(replyModule);

            updateUIElementsToReflectTweet();


        } catch (IOException ex) {

            LOG.error("Error loading element from /fxml/TimelineTwitterModuleFXML.fxml", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

    }

    /**
     * Used to retweet the tweet by opening a second dialog pane to complete task. 
     * This does not increment the number of retweets. 
     * @param event 
     */
    @FXML
    public void retweetWithMessage(ActionEvent event) {

        LOG.info("Attempting to retweet with a comment.");

        if (this.tweet == null) {

            LOG.error("Attempting to retweet this tweet with a message but the tweet object is null");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        try {

            Node sendMessageModule = loadRetweetTweetMessageModule(TwitterAtDawsonProjectConstants.PATH_TO_FXML_SEND_TWEETS);

            //TODO may need to see if need to make it close automatically.
            this.dialogMaker.displayDialogWithNode(sendMessageModule);
            
            updateUIElementsToReflectTweet();

        } catch (IOException ex) {

            LOG.error("Error loading element to retweet tweet with message", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
        }
    }

    /**
     * Used to simply retweet the tweet. It disables the button and increments the count. 
     * 
     * @param event 
     */
    @FXML
    public void retweetWithNoMessage(ActionEvent event) {

        LOG.info("Retweet tweet with no message");

        if (this.tweet == null) {

            LOG.error("Attempting to retweet this tweet with no message but the tweet object is null");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        try {

            this.tweet.retweetTweetWithNoMessage();
            
            updateUIElementsToReflectTweet();

        } catch (TwitterException ex) {

            LOG.error("Unable to retweet the tweet", ex);

            //Notify the user that there was a problem
            this.dialogMaker.displayErrorAlert("Unable_To_Retweet");

        }

        this.setActionToUndoRetweet();
        
    }
    
    /**
     * To undo a retweet with no message. 
     * 
     * @param event 
     */
    public void undoRetweet(ActionEvent event){
        
         LOG.info("Undo retweet tweet with no message");

        if (this.tweet == null) {

            LOG.error("Attempting to undo retweet this tweet with no message but the tweet object is null");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        try {

            this.tweet.unRetweetTweet();

            updateUIElementsToReflectTweet();


        } catch (TwitterException ex) {

            LOG.error("Unable to unretweet the tweet", ex);

            //Notify the user that there was a problem
            this.dialogMaker.displayErrorAlert("Unable_To_UnRetweet");

        }
        
        this.setActionToRetweetWithNoMessage();
        
    }
    
    /**
     * Used to set the retweet action to undo retweet action
     */
    private void setActionToUndoRetweet(){
        
        this.retweetWithNoMessageButton.setOnAction(this::undoRetweet);
        
        //Add style to indicate it is liked
        this.retweetWithNoMessageButton.getStyleClass().addAll("tweet-retweet");
        
    }
    
    /**
     * Used to set the retweet action to retweet with no message
     */
    private void setActionToRetweetWithNoMessage(){
        
        this.retweetWithNoMessageButton.setOnAction(this::retweetWithNoMessage);
        
        //Reset style back to normal
        this.retweetWithNoMessageButton.getStyleClass().clear();
        this.retweetWithNoMessageButton.getStyleClass().addAll("button", "tweet-control-button");
        
    }
    
    /**
     * Used to set the save to database button to delete from the database. 
     */
    private void setActionToDeleteFromDatabase(){
        
        this.saveTweetButton.setOnAction(this::deleteTweetFromDatabase);
        
        //Add style to indicate it is liked
        this.saveTweetButton.getStyleClass().addAll("saved-status");
        
    }
    
    /**
     * Used to set the save to database button to the save to database action. 
     */
    private void setActionToSaveToDatabase(){
        
        this.saveTweetButton.setOnAction(this::saveTweetToDatabase);
        
        //Reset style back to normal
        this.saveTweetButton.getStyleClass().clear();
        this.saveTweetButton.getStyleClass().addAll("button", "tweet-control-button");
        
    }

    /**
     * Used to save the tweet to a local database. 
     * 
     * @param event 
     */
    @FXML
    public void saveTweetToDatabase(ActionEvent event) {
        
        try {
            
            boolean savedStatusToDatabase = this.tweet.saveStatusToDatabase(new TweetDAOManager());
            
            if(savedStatusToDatabase){
                
                //Now set the action to delete the status
                setActionToDeleteFromDatabase();
                updateUIElementsToReflectTweet();
                
            }
            else {
                
                LOG.error("Attempting to save a tweet to the database but no error occured and the result returned was false");
                this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
                Platform.exit();
                
            }
        
        } catch (SQLException | TwitterException | IOException ex) {
            
            LOG.error("Attempting to save a tweet to the database but error occured", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
            
        } 
        
    }
    
    /**
     * Used to delete the given tweet from the database. 
     * 
     * @param event 
     */
    public void deleteTweetFromDatabase(ActionEvent event) {
        
        try {
            
            boolean deletedStatusFromDatabased = this.tweet.deleteStatusFromTheDatabase(new TweetDAOManager());
            
            if(deletedStatusFromDatabased){
                
                //Now make button be a save instead
                setActionToSaveToDatabase();
                updateUIElementsToReflectTweet();
                
            }
            else {
                
                LOG.error("Attempting to delete tweet from the database but no error occured and the result returned was false");
                this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
                Platform.exit();
                
            }
        
        } catch (SQLException | TwitterException | IOException ex) {
            
            LOG.error("Attempting to delete tweet from the database but error occured", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
            
        }
        
    }

    /**
     * Used to view the profile of the user associated with the tweet. 
     * 
     * @param event 
     */
    @FXML
    public void viewUserProfile(ActionEvent event) {
        
        LOG.info("Viewing user profile");

        if (this.tweet == null) {

            LOG.error("Attempting to view user of this tweet but the tweet object is null");
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }

        try {

            DialogPane userProfileDialogPane = this.getUserProfileDialogPane();

            //Automatically shows and prompts user
            this.dialogMaker.displayDialogWithGivenDialogPane(userProfileDialogPane);

        } catch (IOException ex) {

            LOG.error("Error loading FXML element", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }
        
    }

    /**
     * Used to associate the UI with a given tweet.
     * 
     * @param tweet to associate the UI with 
     */
    public void setTweet(TweetModule tweet) {

        this.tweet = tweet;

        updateUIElementsToReflectTweet();

    }

    /**
     * Just initialize the tweet and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    void initialize() {

        assertFXMLInjectionWasSuccessful();

        this.dialogMaker = new DialogMaker(this.resources);

    }

    /**
     * Asserts that all FXML elements are injected correctly into the controller
     */
    private void assertFXMLInjectionWasSuccessful() {

        assert tweetBorderPane != null: "fx:id=\"tweetBorderPane\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert profileImageView != null: "fx:id=\"profileImageView\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert twitterNameLabel != null: "fx:id=\"twitterNameLabel\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert twitterUserHandle != null: "fx:id=\"twitterUserHandle\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert tweetDateLabel != null: "fx:id=\"tweetDateLabel\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert tweetMessage != null: "fx:id=\"tweetMessage\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert numberOfCommentsLabel != null: "fx:id=\"numberOfCommentsLabel\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert numberOfRetweetsLabel != null: "fx:id=\"numberOfRetweetsLabel\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert numberOfLikesLabel != null: "fx:id=\"numberOfLikesLabel\" was not injected: check your FXML file 'TweetFXML.fxml'.";

    }

    /**
     * Used to update the number of comments. 
     */
    private void updateCommentsCount() {

        try {

            this.numberOfCommentsLabel.setText("" + this.tweet.getNumberOfComments());

        } catch (TwitterException ex) {

            LOG.error("Unable to know how many comments associated to tweet.", ex);

        }

    }
    
    /**
     * Set duration of since the tweet was posted 
     */
    private void setDuration(){
        
        Date timeOfTweet = this.tweet.getDatePosted();
        Date now = new Date();
        
        String since = "" + TimeUnit.MILLISECONDS.toMinutes(now.getTime() - timeOfTweet.getTime());
        
        this.tweetDateLabel.setText(since);
        
    }


    /**
     * Used to update the UI elements to reflect the tweet
     */
    private void updateUIElementsToReflectTweet() {

        if (this.tweet == null) {

            LOG.warn("Attempting to update the UI but the tweet has not been set!");
            return;

        }

        //State of like
        if (this.tweet.isTweetLiked()) {

            this.setActionToUnlike();

        }

        //State of retweet
        if (this.tweet.isTweetRetweetedMe()) {

            this.setActionToUndoRetweet();

        }

        //Totals
        updateCommentsCount();

        this.numberOfLikesLabel.setText("" + this.tweet.getNumberOfLikes());
        this.numberOfRetweetsLabel.setText("" + this.tweet.getNumberOfRetweets());

        //User
        this.twitterNameLabel.setText(this.tweet.getUserName());
        this.twitterUserHandle.setText("@" + this.tweet.getTwitterHandle());
        
        this.tweetDateLabel.setText(this.tweet.getDatePosted().toString());

        //User profile image
        Image profileImage = new Image(this.tweet.getUserProfileImageURL(), 75, 30, true, false);
        this.profileImageView.setImage(profileImage);

        //Actual tweet 
        this.tweetMessage.setText(this.tweet.getTweetText());
        
        //Databse state
        try {
            
            //Already saved so should delete
            if (this.tweet.isSavedInDatabase(new TweetDAOManager())){
                
                this.setActionToDeleteFromDatabase();
                
            }
            
        } catch (SQLException | TwitterException | IOException ex) {
            
            LOG.error("Error checking if the tweet is already saved or not", ex);
        
        }

    }

    /**
     * Method dealing with the logic of loading the reply dialog pane
     * 
     * @return A DialogPane node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private DialogPane getReplyDialogPane() throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(TweetFXMLController.class.getResource("/fxml/ReplyTweetFXML.fxml"));

        //Give message bundles and other resources
        loader.setResources(this.resources);

        DialogPane node = loader.load();

        ReplyTweetFXMLController controller = loader.getController();

        //Give reference to the Tweet module 
        controller.setTweetModule(this.tweet);

        return node;

    }
    
    /**
     * Method dealing with the logic of loading the reply dialog pane
     * 
     * @return A Dialog node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private DialogPane getUserProfileDialogPane() throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(TweetFXMLController.class.getResource("/fxml/UserProfleFXML.fxml"));

        //Give message bundles and other resources
        loader.setResources(this.resources);

        DialogPane node = loader.load();

        UserProfileFXMLController controller = loader.getController();

        //Give reference to the Tweet module 
        controller.setUser(new TwitterUser(this.tweet.getUserReference()));

        return node;

    }

    /**
     * Method dealing with the logic of loading the send tweet module and giving it reference to 
     * the tweet url. 
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node loadRetweetTweetMessageModule(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(TweetFXMLController.class.getResource(path));

        //Give message bundles and other resources
        loader.setResources(this.resources);

        Node node = loader.load();

        SendTweetFXMLController controller = loader.getController();

        controller.setRetweetURL(this.tweet.getTweetURL());

        return node;

    }

}