package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.MentionsTimelineApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.slf4j.LoggerFactory;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

/**
 * 
 * Displays tweets that mention the user
 * 
 * @author Jeffrey Boisvert
 */
public class TweetMentionsFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mentionsBorderPaneModule"
    private BorderPane mentionsBorderPaneModule; // Value injected by FXMLLoader

    private DialogMaker dialogMaker;

    private TimelineListViewController timelineController;

    /**
     * Just initialize the form and inject FXML properties. Acts as a kind of constructor
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    public void initialize() {

        assert mentionsBorderPaneModule != null: "fx:id=\"mentionsBorderPaneModule\" was not injected: check your FXML file 'TweetMentionsFXML.fxml'.";

        setTimelineListView();

        loadMentions();

        this.dialogMaker = new DialogMaker(this.resources);

    }

    /**
     * Used to load the mentions
     */
    private void loadMentions() {

        LOG.info("Loading tweets in mentions");

        this.timelineController.loadAndRefreshTimeline();

    }



    /**
     * Method dealing with the logic of loading an JavaFX Parent node from a FXML file
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Parent getElementFromFXMLLoader(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        Parent node = loader.load();

        return node;

    }

    /**
     * Used to set the timeline list view which will be used to set the TwitterTimeineModule to be used in that controller.
     * This also gives reference to the controller to be later used. 
     */
    private void setTimelineListView() {

        try {

            Node twitterModule = getTimelineElement(TwitterAtDawsonProjectConstants.PATH_TO_TIMELINE_LIST_VIEW_FXML);

            this.mentionsBorderPaneModule.setCenter(twitterModule);

        } catch (IOException ex) {

            LOG.error("Error loading fxml in setSendTweetsModule", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();

        }
    }

    /**
     * Method dealing with the logic of setting the controller and loading the timeline UI element
     * 
     * @param path Path to the FXML file (ex: /fxml/MainLeftMenuFXML.fxml)
     * @return A Parent node of the object loaded from the FXMLLoader
     * @throws IOException if file is not found from path
     */
    private Node getTimelineElement(String path) throws IOException {

        //Load FXML from given path
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));

        //Give message bundles and other resources
        loader.setResources(resources);

        Node node = loader.load();

        //Get reference to the controller
        this.timelineController = loader.getController();

        //Set the controller to be using the home timeline app. 
        this.timelineController.setTimelineModule(new MentionsTimelineApp());

        return node;

    }

}