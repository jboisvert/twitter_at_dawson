package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TweetStatus;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import org.slf4j.LoggerFactory;

/**
 * Used to encapsulate a TwitterModule into a ListView. 
 * 
 * @author Jeffrey Boisvert
 */
public class TweetTileCell extends ListCell < TweetModule > {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetTileCell.class);

    private final ResourceBundle resources;

    public TweetTileCell(ResourceBundle resources) {

        this.resources = resources;

    }

    /**
     * This method is called when ever cells need to be updated
     *
     * @param tweet to be encapsulated in graphic
     * @param empty
     */
    @Override
    protected void updateItem(TweetModule tweet, boolean empty) {

        super.updateItem(tweet, empty);

        LOG.info("updateItem in UserTile");

        if (empty || tweet == null) {

            setText(null);
            setGraphic(null);

        } else {

            try {

                LOG.info("Setting graphic for tile");
                setGraphic(getTweetGraphic(tweet));

            } catch (IOException ex) {

                LOG.error("Error loading graphic for Tweet", ex);

            }

        }
    }

    /**
     * Does logic so UI element encapsulates a user
     * 
     * @return UI Node to represent a UserTile 
     */
    private Node getTweetGraphic(TweetModule tweet) throws IOException {

        //Load graghic
        FXMLLoader loader = new FXMLLoader(UserTileCell.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_TWEET));

        //Set the language 
        loader.setResources(this.resources);

        Node root = loader.load();

        //Disable button so user is forced to enter valid data
        TweetFXMLController controller = loader.getController();

        //Needs to be done or else user cannot close app from login screen
        controller.setTweet(tweet);

        return root;

    }

}