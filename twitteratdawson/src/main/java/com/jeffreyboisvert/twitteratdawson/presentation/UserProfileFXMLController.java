package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.business.TwitterUser;
import com.jeffreyboisvert.twitteratdawson.business.TwitterUserModule;
import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Used for the UI of the User. This is where a user can view basic info about a user 
 * which includes functionality such as sending a direct message and following/unfollowing a user.
 * 
 * @author Jeffrey Boisvert
 */
public class UserProfileFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserProfileFXMLController.class);
    
    private final static int IMAGE_WIDTH = 150;
    
    private final static int IMAGE_HEIGHT = 150;
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML // fx:id="dialogWindow"
    private DialogPane dialogWindow; // Value injected by FXMLLoader
    
    @FXML // fx:id="topUserElementsHBox"
    private HBox topUserElementsHBox; // Value injected by FXMLLoader

    @FXML // fx:id="profileImageView"
    private ImageView profileImageView; // Value injected by FXMLLoader

    @FXML // fx:id="twitterNameLabel"
    private Label twitterNameLabel; // Value injected by FXMLLoader

    @FXML // fx:id="twitterUserHandle"
    private Label twitterUserHandle; // Value injected by FXMLLoader

    @FXML // fx:id="userStatusLabel"
    private Text userStatusLabel; // Value injected by FXMLLoader
    
    @FXML // fx:id="followButton"
    private Button followButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="messageUserButton"
    private Button messageUserButton; // Value injected by FXMLLoader
    
    //Used to open the direct message
    private MainLeftMenuFXMLController menuController;
    
    private TwitterUserModule user;
    
    private DialogMaker dialogMaker;

    /**
     * Used to follow the user associated to the UI element. 
     * 
     * @param event 
     */
    @FXML
    public void followUser(ActionEvent event) {

        LOG.info("Follow user " + this.user.getHandle());
        
        try {
            
            this.user.follow();
            
            setButtonToUnFollow();
            
        } catch (TwitterException ex) {
            
            LOG.error("Unable to unfollow user", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
        
        }
        
        
    }
    
    /**
     * Used to unfollow the user associated to the UI element. 
     * 
     * @param event holding info about the event triggered 
     */
    public void unfollowUser(ActionEvent event){
        
         LOG.info("Unfollow user " + this.user.getHandle());
         
        try {
            
            this.user.unfollow();
            
            setButtonToFollow();
            
        } catch (TwitterException ex) {
            
            LOG.error("Unable to unfollow user", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
        
        }
        
    }

    /**
     * Used to open the direct message module. 
     * 
     * @param event 
     */
    @FXML
    void messageUser(ActionEvent event) {

        LOG.info("Attempting to message user " + this.user.getHandle());
        
        menuController.openDirectMessageWithHandle(this.user);
        
        //Close the user view
        this.exitWindow();
        
    }
    
    /**
     * Used to set the menu controller to be able to send a direct message if needed.
     * 
     * @param controller 
     */
    public void setMenuController(MainLeftMenuFXMLController controller){
        
        LOG.info("Set menu controller in user profile view");
        
        this.menuController = controller; 
        
    }
    
    /**
     * Used to close the dialog window
     */
    public void exitWindow() {

        final Stage stage = (Stage) this.dialogWindow.getScene().getWindow();

        stage.close();

    }
    
    /**
     * Used to close the dialog. 
     * 
     * @param event triggered 
     */
    @FXML
    public void closeDialog(ActionEvent event) {

        exitWindow();
        
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert profileImageView != null: "fx:id=\"profileImageView\" was not injected: check your FXML file 'UserProfleFXML.fxml'.";
        assert twitterNameLabel != null: "fx:id=\"twitterNameLabel\" was not injected: check your FXML file 'UserProfleFXML.fxml'.";
        assert twitterUserHandle != null: "fx:id=\"twitterUserHandle\" was not injected: check your FXML file 'UserProfleFXML.fxml'.";
        assert userStatusLabel != null: "fx:id=\"userStatusLabel\" was not injected: check your FXML file 'UserProfleFXML.fxml'.";

        this.dialogMaker = new DialogMaker(this.resources);
        
        //Get reference to the controller in memory. 
        this.menuController = MainLeftMenuFXMLController.getSingleton();
        
    
    }
    
    /**
     * Used to set reference to the user object encapsulated in the UI. 
     * 
     * @param user 
     */
    public void setUser(TwitterUserModule user){
        
        this.user = user; 
        
        updateUIToReflectUserAssigned();
        
    }
    
    /**
     * Used to update the UI of the view to reflect the assigned user
     */
    private void updateUIToReflectUserAssigned(){
        
        Image profileImage = new Image(this.user.getProfileImage400X400URL(), IMAGE_WIDTH, IMAGE_HEIGHT, true, false);
        this.profileImageView.setImage(profileImage);
        
        this.twitterUserHandle.setText("@" + this.user.getHandle());
        
        this.userStatusLabel.setText(this.user.getDescription());
        
        try {
            
            //Means that this is the user that is logged in and the user should not be able
            //to follow or message themselves.
            if (this.user.isTheAuthenticatingUser()){
                
                //Remove the follow and message buttons since they are not valid for the logged in user
                this.topUserElementsHBox.getChildren().removeAll(this.followButton, this.messageUserButton);
                
           }
            else {
                
                //Set the state of the user buttons
                setStateOfFollow();
                
                //Can the user message this user or not
                validateIfUserCanMessageThisUser();
                
            }
        } catch (TwitterException ex) {
            
            LOG.error("Issue updating UI to reflect user", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
            
        } 

    }
    
    /**
     * Used to validate if the user in UI is one following the logged in user. 
     * This method removes the message button so the user cannot message them. 
     */
    private void validateIfUserCanMessageThisUser(){
        
       try {
            
            //Means the button should be disabled
            if(!this.user.isFollowingAuthenticatedUser()){
                
                //Remove message button
                this.topUserElementsHBox.getChildren().remove(this.messageUserButton);
                
            }
        
        }
        catch (TwitterException ex) {
            
            LOG.error("Error validating if the user is the logged in user", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
        
        }
        
    }

    /**
     * Used to set the state of the follow button.
     * If the user has not already followed the user it will set it to the follow state.
     * If the user is already following them then the state will be unfollow.
     * This includes the styling and the on action event. 
     */
    private void setStateOfFollow() {
        
        //button-unfollow

       try {
            
            //Means the user is following the user already
            if(this.user.isFollowedByAuthenticatedUser()){
                
                setButtonToUnFollow();
                
            }
        
        }
        catch (TwitterException ex) {
            
            LOG.error("Error validating if the user is the logged in user", ex);
            this.dialogMaker.displayErrorAlert(TwitterAtDawsonProjectConstants.FATAL_ERROR_MESSAGE_KEY);
            Platform.exit();
        
        }
        
    }
    
    /**
     * Takes care of setting the follow button to the unfollow state.
     */
    private void setButtonToUnFollow(){
        
        this.followButton.getStyleClass().add("button-unfollow");
                
        this.followButton.setText(resources.getString("Unfollow_User_Button"));
                
        this.followButton.setOnAction(this::unfollowUser);
        
    }
    
    /**
     * Takes care of setting the follow button to the follow state. 
     */
    private void setButtonToFollow(){
        
        
        this.followButton.getStyleClass().clear();
        this.followButton.getStyleClass().add("button");
                
        this.followButton.setText(resources.getString("Follow_User_Button"));
                
        this.followButton.setOnAction(this::followUser);
        
    }

}