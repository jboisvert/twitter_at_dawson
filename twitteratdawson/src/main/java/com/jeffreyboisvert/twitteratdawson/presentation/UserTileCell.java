package com.jeffreyboisvert.twitteratdawson.presentation;

import com.jeffreyboisvert.twitteratdawson.constants.TwitterAtDawsonProjectConstants;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import org.slf4j.LoggerFactory;
import twitter4j.User;

/**
 * Used to encapsulate a twitter4j User into a ListView. 
 * 
 * @author Jeffrey Boisvert
 */
public class UserTileCell extends ListCell < User > {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserTileCell.class);

    /**
     * This method is called when ever cells need to be updated
     *
     * @param user
     * @param empty
     */
    @Override
    protected void updateItem(User user, boolean empty) {

        super.updateItem(user, empty);
        
        LOG.info("updateItem in UserTile");

        if (empty || user == null) {

            setText(null);
            setGraphic(null);

        } else {

            try {

                LOG.info("Setting graphic for tile");
                setGraphic(getUserTileGraphic(user));

            } catch (IOException ex) {

                LOG.error("Error loading graphic for UserTile");

            }

        }
    }

    /**
     * Does logic so UI element encapsulates a user
     * 
     * @return UI Node to represent a UserTile 
     */
    private Node getUserTileGraphic(User user) throws IOException {

        //Load graghic
        FXMLLoader loader = new FXMLLoader(UserTileCell.class.getResource(TwitterAtDawsonProjectConstants.PATH_TO_FXML_USER_TILE_FXML));

        Node root = loader.load();

        //Disable button so user is forced to enter valid data
        UserTileFXMLController controller = loader.getController();

        //Needs to be done or else user cannot close app from login screen
        controller.setUser(user);

        return root;

    }

}