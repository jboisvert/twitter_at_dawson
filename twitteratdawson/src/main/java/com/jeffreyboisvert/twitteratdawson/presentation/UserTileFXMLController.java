package com.jeffreyboisvert.twitteratdawson.presentation;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import org.slf4j.LoggerFactory;
import twitter4j.User;

/**
 * Simple controller for the User tile UI element. 
 * 
 * @author Jeffrey Boisvert
 */
public class UserTileFXMLController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserTileFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="rootUIElement"
    private BorderPane rootUIElement; // Value injected by FXMLLoader

    @FXML // fx:id="profileImageView"
    private ImageView profileImageView; // Value injected by FXMLLoader

    @FXML // fx:id="twitterUserHandle"
    private Label twitterUserHandle; // Value injected by FXMLLoader

    //User being repersented
    private final ObjectProperty < User > user;

    /**
     * Just used to create ObjectProperty to bind elements to
     */
    public UserTileFXMLController() {

        this.user = new SimpleObjectProperty < > ();

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert profileImageView != null: "fx:id=\"profileImageView\" was not injected: check your FXML file 'UserTileFXML.fxml'.";
        assert twitterUserHandle != null: "fx:id=\"twitterUserHandle\" was not injected: check your FXML file 'UserTileFXML.fxml'.";

    }

    /**
     * Used to update the tile values with what is assigned in the user object
     */
    private void updateValuesOfTile() {

        this.twitterUserHandle.textProperty().set("@" + this.user.get().getScreenName());

        Image profileImage = new Image(this.user.get().getProfileImageURL(), 75, 30, true, false);
        this.profileImageView.setImage(profileImage);

    }

    /**
     * Pass reference of user object
     * 
     * @return reference of user encapsulated in tile 
     */
    public User getUser() {

        return this.user.get();

    }

    /**
     * Used to set the reference of the user.
     * It also updates the UI of the user. 
     * 
     * @param user 
     */
    public void setUser(User user) {

        this.user.set(user);
        updateValuesOfTile();

    }

}