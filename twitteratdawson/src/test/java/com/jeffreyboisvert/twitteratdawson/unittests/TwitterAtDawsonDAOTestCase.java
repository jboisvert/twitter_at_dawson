
package com.jeffreyboisvert.twitteratdawson.unittests;

import com.jeffreyboisvert.twitteratdawson.bean.StatusData;
import com.jeffreyboisvert.twitteratdawson.bean.UserData;
import com.jeffreyboisvert.twitteratdawson.business.TweetModule;
import com.jeffreyboisvert.twitteratdawson.persistence.TwitterAtDawsonDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.Test;

import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;

/**
 * Used to test the DAO object in a test database. 
 * 
 * @author Jeffrey Boisvert
 */
public class TwitterAtDawsonDAOTestCase {
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterAtDawsonDAOTestCase.class);
        
    private final static String URL = "jdbc:mysql://localhost:3306/TEST_TWITTER_TWEETS?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    // The MySQL USER must have Drop Table and Create Table privileges
    private final static String USER = "test";
    private final static String PASSWORD = "password";
    
    /**
     * Used to test if a status is made given a valid StatusData object and a valid 
     * author id (already in database).
     * 
     * @throws java.sql.SQLException which causes the test case to fail.
     */
    @Test
    public void testCreateStatus() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(5, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int rowsAffected = dao.createStatus(data, 1);
        
        //Then
        assertEquals("testCreateStatus: ", 1, rowsAffected);

    }
    
    /**
     * Used to test if an SQLException is thrown if trying to insert a status with an author
     * that is not in the database. 
     * 
     * @throws java.sql.SQLException which causes the test case to fail.
     */
    @Test(expected=SQLException.class) 
    public void testCreateStatusWithAuthorNotInDatabase() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(5, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createStatus(data, 5);

    }
    
    /**
     * Used to test if an SQLException is thrown if trying to insert a status
     * with the same id as a status in the database. 
     * 
     * @throws java.sql.SQLException which causes the test case to fail.
     */
    @Test(expected=SQLException.class) 
    public void testCreateStatusIdlreadyInDatabase() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createStatus(data, 1);

    }
    
    /**
     * Used to test if a valid Author can be inserted into the database. 
     * 
     * @throws SQLException 
     */
    @Test
    public void testCreateTweetAuthor() throws SQLException {
        
        //Given
        UserData data = new UserData(5, "Test", "Test1234567", "https://google.ca/green.jpg");
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int rowsAffected = dao.createTweetAuthor(data);
        
        //Then
        assertEquals("testCreateTweetAuthor: ", 1, rowsAffected);

    }
    
    /**
     * Used to test if an existing Author can be inserted into the database 
     * or not and throw an error with the same id.
     * 
     * @throws SQLException 
     */
    @Test(expected=SQLException.class) 
    public void testCreateTweetAuthorWithSameIdAlreadyInDatabase() throws SQLException {
        
        //Given
        UserData data = new UserData(1, "Test", "Test1234567", "https://google.ca/green.jpg");
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createTweetAuthor(data);

    }
    
    /**
     * Used to test if an existing Author can be inserted into the database 
     * or not and throw an error with the same screen name.
     * 
     * @throws SQLException 
     */
    @Test(expected=SQLException.class) 
    public void testCreateTweetAuthorWithScreenNameAlreadyInDatabase() throws SQLException {
        
        //Given
        UserData data = new UserData(1, "Tester123", "Test1234567", "https://google.ca/green.jpg");
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createTweetAuthor(data);

    }
    
    /**
     * Used to test if a valid user can be inserted into the database. 
     * 
     * @throws SQLException 
     */
    @Test
    public void testCreateUser() throws SQLException {
        
        //Given
        UserData data = new UserData(123, "Test", "Test1234567", "https://google.ca/green.jpg");
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int rowsAffected = dao.createTweetAuthor(data);
        
        //Then
        assertEquals("testCreateUser: ", 1, rowsAffected);

    }
    
    /**
     * Used to test if inserting a user that already exists throws an error. 
     * 
     * @throws SQLException 
     */
    @Test(expected=SQLException.class) 
    public void testCreateUserWithIdAlreadyInDatabase() throws SQLException {
        
        //Given
        UserData data = new UserData(1, "Test", "Test1234567", "https://google.ca/green.jpg");
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createTweetAuthor(data);

    }
    
    /**
     * Used to test if a valid status (existing) and valid user id (existing) can be inserted.
     * @throws SQLException 
     */
    @Test
    public void testCreateUserTweetRelationship() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int rowsAffected = dao.createUserTweetRelationship(data, 1);
        
        //Then
        assertEquals("testCreateUserTweetRelationship: ", 1, rowsAffected);

    }
    
    /**
     * Used to test if an error is throw when attempting to create a relationship with a tweet that does not exist of that id.
     * 
     * @throws SQLException 
     */
    @Test(expected=SQLException.class) 
    public void testCreateUserTweetRelationshipWithTweetThatDoesNotExist() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(500, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createUserTweetRelationship(data, 1);

    }
    
    /**
     * Used to test if an error is thrown when trying to insert a relationship with 
     * a user that does not exist in the database
     * 
     * @throws SQLException 
     */
    @Test(expected=SQLException.class) 
    public void testCreateUserTweetRelationshipWithUserThatDoesNotExist() throws SQLException {
        
        //Given
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "test", 5, 1, true, false);
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.createUserTweetRelationship(data, 500);

    }
    
    /**
     * Used to test if all the tweets that a user saved is returned after inserting
     * 
     * @throws SQLException 
     */
    @Test
    public void testFindAllSavedStatusOfUserAfterInserting() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "test", 5, 1, true, false);
        
        //When
        dao.createUserTweetRelationship(data, 1);
        List<TweetModule> tweets = dao.findAllSavedStatusOfUser(1);
        
        //Then
        assertEquals("testFindAllSavedStatusOfUserAfterInserting: ", 3, tweets.size());

    }
    
    /**
     * Used to test if all the tweets that a user saved is returned after deleting one.
     * 
     * @throws SQLException 
     */
    @Test
    public void testFindAllSavedStatusOfUserAfterDeleting() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        dao.deleteUserTweetsRelationship(1, 1);
        List<TweetModule> tweets = dao.findAllSavedStatusOfUser(1);
        
        //Then
        assertEquals("testFindAllSavedStatusOfUserAfterInserting: ", 1, tweets.size());

    }
    
    /**
     * Used to test if method works with a user that does not exist in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testFindAllSavedStatusOfUserWithUserThatDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        List<TweetModule> tweets = dao.findAllSavedStatusOfUser(555);
        
        //Then
        assertEquals("testFindAllSavedStatusOfUserWithUserThatDoesNotExist: ", 0, tweets.size());

    }
    
    /**
     * Used to test if method works with a user not already in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsUserAlreadyCreated() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        boolean result = dao.isUserAlreadyCreated(1);
        
        //Then
        assertTrue(result);

    }
    
    /**
     * Used to test if method works with a user not already in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsUserAlreadyCreatedWhenDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        boolean result = dao.isUserAlreadyCreated(555);
        
        //Then
        assertFalse(result);

    }
    
    /**
     * Used to test if method works with a user already in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsAuthorAlreadyCreated() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        UserData data = new UserData(1, "Tester", "Tester123", "https:test.com/test.jpg");
        
        //When
        boolean result = dao.isAuthorAlreadyCreated(data);
        
        //Then
        assertTrue(result);

    }
    
    /**
     * Used to test if method works with a user not already in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsAuthorAlreadyCreatedWhenDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        UserData data = new UserData(555, "Tester", "greeen", "https:test.com/test.jpg");
        
        //When
        boolean result = dao.isAuthorAlreadyCreated(data);
        
        //Then
        assertFalse(result);

    }
    
    /**
     * Used to test if method works with a tweet already in the database.
     * This is validated if id is already taken. 
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsTweetAlreadyCreated() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "Hello World", 1, 1, true, false);
        
        //When
        boolean result = dao.isTweetAlreadyCreated(data);
        
        //Then
        assertTrue(result);

    }
    
    /**
     * Used to test if method works with a tweet not already in the database.
     * this is validated if the id is not already in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsTweetAlreadyCreatedWhenDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(555, created, "test", 5, 1, true, false);
        
        //When
        boolean result = dao.isTweetAlreadyCreated(data);
        
        //Then
        assertFalse(result);

    }
    
    /**
     * Used to test if method works when a user already saved a tweet.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsTweetAlreadySavedByUser() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
                boolean result = dao.isTweetAlreadySavedByUser(1,1);
        
        //Then
        assertTrue(result);

    }
    
    /**
     * Used to test if method works with a tweet the user has not saved in the database.
     * 
     * @throws SQLException 
     */
    @Test
    public void testIsTweetAlreadySavedByUserWhenDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        boolean result = dao.isTweetAlreadySavedByUser(1,555);
        
        //Then
        assertFalse(result);

    }
    
    /**
     * Used to test if user is able to update a tweet correctly.
     * 
     * @throws SQLException 
     */
    @Test
    public void testUpdateStatus() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(1, created, "Bye World", 1, 1, true, false);
        
        //When
        int result = dao.updateStatus(data);
        
        //Then
        assertEquals("testUpdateStatus: ", 1, result);

    }
    
    /**
     * Used to test if attempting to update a status that does not exist returns 0. 
     * 
     * @throws SQLException 
     */
    @Test
    public void testUpdateStatusThatDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        Date created =  new GregorianCalendar(2019, Calendar.FEBRUARY, 12).getTime();
        StatusData data = new StatusData(555, created, "Hello World Does Not Exist", 1, 1, true, false);
        
        //When
        int result = dao.updateStatus(data);
        
        //Then
        assertEquals("testUpdateStatusThatDoesNotExist: ", 0, result);

    }
    
    /**
     * Used to test if able to delete a relationship with a user and a tweet (unsave).
     * 
     * @throws SQLException 
     */
    @Test
    public void testDeleteUserTweetsRelationship() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int result = dao.deleteUserTweetsRelationship(1,1);
        
        //Then
        assertEquals("testDeleteUserTweetsRelationship: ", 1, result);

    }
    
    /**
     * Used to test if zero is returned when attempting to delete a reltionship that does not exist.
     * 
     * @throws SQLException 
     */
    @Test
    public void testDeleteUserTweetsRelationshipThatDoesNotExist() throws SQLException {
        
        //Given
        TwitterAtDawsonDAO dao = new TwitterAtDawsonDAO(URL, USER, PASSWORD);
        
        //When
        int result = dao.deleteUserTweetsRelationship(1,55);
        
        //Then
        assertEquals("testDeleteUserTweetsRelationshipThatDoesNotExist: ", 0, result);

    }
    
    
    /**
     * The database is recreated before each test. If the last test is
     * destructive then the database is in an unstable state. @AfterClass is
     * called just once when the test class is finished with by the JUnit
     * framework. It is instantiating the test class anonymously so that it can
     * execute its non-static seedDatabase routine.
     */
    @AfterClass
    public static void seedAfterTestCompleted() {
        LOG.info("@AfterClass seeding");
        new TwitterAtDawsonDAOTestCase().seedDatabase();
    }
    
   /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    @Before
    public void seedDatabase() {
        LOG.info("@Before seeding");

        final String createTables = loadAsString("createTwitterTweetSchema.sql");
        final String seedDataScript = loadAsString("insertTestDataIntoTwitterTweetSchema.sql");
        
        //Recreating the database schema.
        runSQLScript(createTables);
        
        //Inserting test data
        runSQLScript(seedDataScript);
        
    }
    
    /**
     * Used to run the given script in the wanted database. 
     * @param given script name to run
     */
    private void runSQLScript(String script){
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);) {
            for (String statement : splitStatements(new StringReader(script), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed running script", e);
        }
        
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
        
    }

    /**
     * Used to split the sql statements
     * 
     * @param reader
     * @param statementDelimiter
     * @return List of String of statements.
     * @author Ken Fogel
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
        
    }

    /**
     * Used to validate if the given string is a comment in SQL. 
     * @param line
     * @return true if it is a comment.
     * @author Ken Fogel
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
    
}
