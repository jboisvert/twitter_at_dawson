-- Best practice MySQL as of 5.7.6
--
-- This handles creating the database for the tests. 
--
-- This script needs to run only once
DROP DATABASE IF EXISTS TEST_TWITTER_TWEETS;
CREATE DATABASE TEST_TWITTER_TWEETS;

USE TEST_TWITTER_TWEETS;

DROP USER IF EXISTS test@localhost;
CREATE USER test@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON TEST_TWITTER_TWEETS.* TO test@'localhost';

FLUSH PRIVILEGES;

