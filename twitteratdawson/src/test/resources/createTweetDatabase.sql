-- Best practice MySQL as of 5.7.6
--
-- The Twitter Tweets database must exist before running this script
-- and before running the twitter_at_dawson because the unit
-- test runs on this database. Only the root user can create a MySQL database
-- but you do not want to use the root user and password in your code.
--
-- This script needs to run only once

DROP DATABASE IF EXISTS TWITTER_TWEETS;
CREATE DATABASE TWITTER_TWEETS;

USE TWITTER_TWEETS;

DROP USER IF EXISTS demo@localhost;
CREATE USER demo@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON TWITTER_TWEETS.* TO demo@'localhost';

FLUSH PRIVILEGES;
