-- Best practice MySQL as of 5.7.6
--
-- The TwitterTweets database must exist before running this script. Use this 
-- script once. This script will now run during unit testing.
DROP TABLE IF EXISTS TWEETS_USERS; 
DROP TABLE IF EXISTS TWEETS; 
DROP TABLE IF EXISTS TWEET_AUTHORS;
DROP TABLE IF EXISTS USERS; 

-- Used to create the USERS table which handles 
-- info for logged in (authenticated users) of the Twitter At Dawson app. 
CREATE TABLE USERS (
  ID int NOT NULL auto_increment,
  TWITTER_ID bigint NOT NULL UNIQUE,  
  PRIMARY KEY  (ID)
) ENGINE=InnoDB;

-- Used to create the TWEET_AUTHORS table which handles 
-- info for authors of tweets. This helps to normalize data and remove redundant data. 
-- Sizes are based on https://help.twitter.com/en/managing-your-account/twitter-username-rules.
-- Profile image URL is 255 since this is unpredictable for URL size (just large value). 
CREATE TABLE TWEET_AUTHORS (
  ID int NOT NULL auto_increment,
  TWITTER_ID bigint NOT NULL UNIQUE,  
  NAME varchar(50) NOT NULL default '',
  SCREEN_NAME varchar(15) NOT NULL, 
  IMAGE_URL varchar(255) NOT NULL,
  PRIMARY KEY  (ID)
) ENGINE=InnoDB;

-- Used to create the TWEETS table which handles 
-- info for authors of tweets. This helps to normalize data and remove redundant data. 
-- A tweet must belong to a user and a tweet's text cannot be larger than 20 characters.
CREATE TABLE TWEETS (
  ID int NOT NULL auto_increment,
  TWEET_ID bigint NOT NULL UNIQUE,
  TWITTER_ID bigint NOT NULL,   
  CREATED_AT datetime NOT NULL,
  TEXT varchar(280) NOT NULL, 
  FAV_COUNT int NOT NULL default 0,
  RETWEET_COUNT int NOT NULL default 0,
  PRIMARY KEY  (ID),
  FOREIGN KEY (TWITTER_ID) REFERENCES TWEET_AUTHORS(TWITTER_ID) ON DELETE CASCADE
) ENGINE=InnoDB;

-- Used to create the TWEETS_USERS table which handles 
-- info for the logged in users and the tweets they have saved. 
-- This helps to normalize data and remove redundant data. 
-- This table acts a bridging table but also holds additional data about if the logged in user
-- like the tweet or retweeted it since this belongs to the relation of the tweet and the user. 
CREATE TABLE TWEETS_USERS (
  ID int NOT NULL auto_increment,
  TWITTER_ID bigint NOT NULL, 
  TWEET_ID bigint NOT NULL,  
  LIKED BOOLEAN NOT NULL default FALSE,
  REWTWEETED BOOLEAN NOT NULL default FALSE,
  PRIMARY KEY  (ID, TWITTER_ID, TWEET_ID),
  FOREIGN KEY (TWITTER_ID) REFERENCES USERS(TWITTER_ID),
  FOREIGN KEY (TWEET_ID) REFERENCES TWEETS(TWEET_ID)
) ENGINE=InnoDB;