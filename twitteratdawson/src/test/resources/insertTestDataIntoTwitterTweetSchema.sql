-- Best practice MySQL as of 5.7.6
--
-- The Twitter Tweets Test database must exist before running this script. Use the
-- createTwitterTweetSchema.sql script once. This script will now run during unit
-- testing.
--
--Test data is kept simple.

--Users
INSERT INTO USERS (TWITTER_ID) 
VALUES (1), (2);

--Tweet Authors 
INSERT INTO TWEET_AUTHORS (TWITTER_ID, NAME, SCREEN_NAME, IMAGE_URL) VALUES 
(1,"Tester","Tester123","https:test.com/test.jpg"),
(2,"Tester2","Tester1234","https:test.com/test2.jpg");

--Tweets
INSERT INTO TWEETS (TWEET_ID, TWITTER_ID, CREATED_AT, TEXT, FAV_COUNT, RETWEET_COUNT) VALUES 
(1,1,'2019-11-06 13:17:17','Hello World',1,0),
(2,2,'2019-11-06 10:17:17','Test Tweet 1',0,1),
(3,2,'2019-11-06 14:17:17','Test Tweet 2',1,1);

--Users who saved tweets
INSERT INTO TWEETS_USERS (TWITTER_ID, TWEET_ID, LIKED, REWTWEETED) VALUES 
(1, 1, TRUE, FALSE),
(1, 2, FALSE, TRUE),
(2, 3, TRUE, TRUE);
